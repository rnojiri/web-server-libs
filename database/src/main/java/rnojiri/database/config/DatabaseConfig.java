package rnojiri.database.config;

import java.beans.PropertyVetoException;

import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.annotation.Order;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import rnojiri.lib.crypt.AesEncryptor;
import rnojiri.lib.util.PropertiesManager;
import rnojiri.lib.util.PropertiesReader;
import rnojiri.lib.util.UnsupportedTypeException;

/**
 * The domain configuration class.
 * 
 * @author rnojiri
 */
@Order(0)
@Configuration
public class DatabaseConfig
{
	private DataSource dataSource;
	
	private AesEncryptor aesEncryptor;
	
	/**
	 * Creates the AES encryptor.
	 * 
	 * @return
	 */
	@Bean
	public AesEncryptor aesEncryptor()
	{
		if(aesEncryptor == null)
		{
			PropertiesReader properties = PropertiesManager.getInstance().getMainProperties();
			
			aesEncryptor = new AesEncryptor(properties.getProperty("encryption.seed"));
		}
		
		return aesEncryptor;
	}
	
	@Bean(destroyMethod="close")
	public DataSource dataSource() throws PropertyVetoException, UnsupportedTypeException
	{
		if(dataSource != null) return dataSource;
		
		PropertiesReader properties = PropertiesManager.getInstance().getMainProperties();
		
		String password = properties.getProperty("jdbc.password");
		
		if(StringUtils.isNotBlank(password))
		{
			password = aesEncryptor().decrypt(password);
		}
		
		ComboPooledDataSource comboPooledDataSource = new ComboPooledDataSource();
		comboPooledDataSource.setDriverClass("com.mysql.cj.jdbc.Driver");
		comboPooledDataSource.setJdbcUrl(properties.getProperty("jdbc.url"));
		comboPooledDataSource.setUser(properties.getProperty("jdbc.user"));
		comboPooledDataSource.setPassword(password);
		comboPooledDataSource.setInitialPoolSize(2);
		comboPooledDataSource.setMinPoolSize(2);
		comboPooledDataSource.setMaxPoolSize(10);
		comboPooledDataSource.setCheckoutTimeout(properties.getCastedProperty(Integer.class, "jdbc.timeout"));
		comboPooledDataSource.setIdleConnectionTestPeriod(5000);
		comboPooledDataSource.setAcquireIncrement(1);
		comboPooledDataSource.setMaxStatements(10);
		comboPooledDataSource.setNumHelperThreads(3);

		dataSource = comboPooledDataSource;
		
		return dataSource;
	}

	@Bean
	@DependsOn("dataSource")
	@Autowired
	public DataSourceTransactionManager transactionManager(DataSource dataSource) throws PropertyVetoException
	{
		return new DataSourceTransactionManager(dataSource);
	}

	@Bean
	@DependsOn("dataSource")
	@Autowired
	public SqlSessionFactory sqlSessionFactory(DataSource dataSource) throws Exception
	{
		SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
		sessionFactory.setConfigurationProperties(PropertiesManager.getInstance().getMainProperties());
		sessionFactory.setDataSource(dataSource);
		
		return sessionFactory.getObject();
	}
	
	@Bean
	public MapperScannerConfigurer mapperScannerConfigurer()
	{
		PropertiesReader properties = PropertiesManager.getInstance().getMainProperties();
		
		MapperScannerConfigurer mapperScannerConfigurer = new MapperScannerConfigurer();
		mapperScannerConfigurer.setProcessPropertyPlaceHolders(true);
		mapperScannerConfigurer.setBasePackage(properties.getProperty("jdbc.mappers"));
		
		return mapperScannerConfigurer;
	}
}
