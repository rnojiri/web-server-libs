package rnojiri.database.transaction.exception;

import java.util.Arrays;

/**
 * Raised when a key was not found in database.
 * 
 * @author rnojiri
 */
public class KeyNotFoundException extends Exception
{
	private static final long serialVersionUID = 5842499438372327512L;

	/**
	 * @param key
	 * @param table
	 */
	public KeyNotFoundException(String key, String table)
	{
		super("Key \"" + key + "\" was not found in table \"" + table + "\".");
	}
	
	/**
	 * @param key
	 * @param table
	 */
	public KeyNotFoundException(Long key, String table)
	{
		this(String.valueOf(key), table);
	}
	
	/**
	 * @param key
	 * @param table
	 */
	public KeyNotFoundException(Integer key, String table)
	{
		this(String.valueOf(key), table);
	}
	
	/**
	 * @param key
	 * @param table
	 */
	public KeyNotFoundException(Byte key, String table)
	{
		this(String.valueOf(key), table);
	}
	
	/**
	 * @param keys
	 * @param table
	 */
	public KeyNotFoundException(Long keys[], String table)
	{
		this(Arrays.toString(keys), table);
	}
	
	/**
	 * @param keys
	 * @param table
	 */
	public KeyNotFoundException(Integer keys[], String table)
	{
		this(Arrays.toString(keys), table);
	}
	
	/**
	 * @param keys
	 * @param table
	 */
	public KeyNotFoundException(Byte keys[], String table)
	{
		this(Arrays.toString(keys), table);
	}
}
