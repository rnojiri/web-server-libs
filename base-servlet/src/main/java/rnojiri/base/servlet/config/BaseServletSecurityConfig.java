package rnojiri.base.servlet.config;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.shiro.web.filter.authc.BasicHttpAuthenticationFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import rnojiri.base.servlet.filter.ReferrerFilter;
import rnojiri.base.servlet.service.UrlManager;
import rnojiri.lib.log.BeanLogger;
import rnojiri.lib.shiro.config.AbstractSecurityConfig;
import rnojiri.lib.shiro.filter.FilterConfig;
import rnojiri.lib.shiro.filter.FilterConfigList;
import rnojiri.lib.util.PropertiesManager;
import rnojiri.lib.util.PropertiesReader;

/**
 * Extends the security configuration.
 * 
 * @author rnojiri
 */
@Configuration
public class BaseServletSecurityConfig extends AbstractSecurityConfig
{
	private static Logger LOGGER = LoggerFactory.getLogger(BaseServletSecurityConfig.class);
	
	private UrlManager urlManager;
	
	/**
	 * Creates the URL manager bean.
	 * 
	 * @return UrlManager
	 */
	@Bean
	@Autowired
	public UrlManager urlManager()
	{
		if(urlManager != null) return urlManager;
		
		PropertiesReader properties = PropertiesManager.getInstance().getMainProperties();
		
		BeanLogger.creating(UrlManager.class);
		
		Set<Pattern> patternSet = new HashSet<>(); 
		
		String nonReferrableUriPatterns[] = properties.getArray("non.referrable.uri.patterns");
		
		if(nonReferrableUriPatterns != null && nonReferrableUriPatterns.length >= 0)
		{
			for(String patternStr : nonReferrableUriPatterns)
			{
				LOGGER.info("Adding non referrable uri pattern: {}", patternStr);
				
				patternSet.add(Pattern.compile(patternStr, Pattern.CASE_INSENSITIVE));
			}
		}
		
		urlManager = new UrlManager(properties.getProperty("default.redirect.url"), patternSet);
		
		return urlManager;
	}
	
	@Override
	protected FilterConfigList getFilters()
	{
		PropertiesReader properties = PropertiesManager.getInstance().getMainProperties();
		
		FilterConfigList filterConfigList = new FilterConfigList();
		
		try
		{
			if((Boolean)properties.getCastedProperty(Boolean.class, "basic.http.auth.enable"))
			{
				filterConfigList.add(new FilterConfig("basicAuthHttpFilter", properties.getProperty("basic.http.auth.context", "/**"), new BasicHttpAuthenticationFilter()));
			}
		}
		catch (Exception e) 
		{
			LOGGER.error("Error reading \"basic.http.auth.enable\" property.", e);
		}
		
		filterConfigList.add(new FilterConfig("referrerFilter", "/**", new ReferrerFilter(urlManager())));
		
		return filterConfigList;
	}
}
