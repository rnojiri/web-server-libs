package rnojiri.base.servlet.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpHeaders;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.filter.GenericFilterBean;

import rnojiri.base.servlet.service.UrlManager;
import rnojiri.lib.shiro.cache.SessionAttributes;

/**
 * A filter to save the referrer.
 * 
 * @author rnojiri
 */
public class ReferrerFilter extends GenericFilterBean
{
	private static final Logger LOGGER = LoggerFactory.getLogger(ReferrerFilter.class);
	
	private final UrlManager urlManager;
	
	/**
	 * @param urlManager
	 */
	public ReferrerFilter(UrlManager urlManager)
	{
		this.urlManager = urlManager;
	}
	
	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException
	{
		try
		{
			HttpServletRequest request = (HttpServletRequest)servletRequest;
		
			final String safeReferrer = urlManager.getReferrableUri(request.getHeader(HttpHeaders.REFERER));
		
			SecurityUtils.getSubject().getSession().setAttribute(SessionAttributes.REFERRER, safeReferrer);
		
			filterChain.doFilter(servletRequest, servletResponse);
		}
		catch(Exception e)
		{
			LOGGER.error("Error executing filter.", e);
			
			throw new ServletException(e);
		}
	}
}
