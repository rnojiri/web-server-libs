package rnojiri.base.servlet.controller.form;

import rnojiri.lib.validation.PostValidation;
import rnojiri.lib.validation.Validable;
import rnojiri.lib.validation.annotation.RequiredParam;

/**
 * The main login form.
 * 
 * @author rnojiri
 */
public class SignInForm implements Validable, EmailField
{
	private String email;
	
	private String password;
	
	private String redirect;
	
	public SignInForm()
	{
		;
	}

	@Override
	public String getEmail()
	{
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email)
	{
		this.email = email;
	}

	/**
	 * @return the password
	 */
	@RequiredParam
	public String getPassword()
	{
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password)
	{
		this.password = password;
	}

	/**
	 * @return the redirect
	 */
	public String getRedirect()
	{
		return redirect;
	}

	/**
	 * @param redirect the redirect to set
	 */
	public void setRedirect(String redirect)
	{
		this.redirect = redirect;
	}

	@Override
	public PostValidation<? extends Validable>[] getPostValidations()
	{
		return null;
	}
}
