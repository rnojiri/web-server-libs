package rnojiri.base.servlet.controller.form;

/**
 * The registration form.
 * 
 * @author rnojiri
 */
public class RegistrationForm extends SignInForm implements UserNameFields
{
	private String firstName;
	
	private String lastName;
	
	public RegistrationForm()
	{
		;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName()
	{
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName()
	{
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}
}
