package rnojiri.lib.shiro.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.subject.Subject;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import rnojiri.lib.support.CommonCodeResults;
import rnojiri.lib.support.JsonResult;

/**
 * Intercepts the login action to return a json result on demand.
 * 
 * @author rnojiri
 */
public class NotAuthorizedJsonResultInterceptor extends HandlerInterceptorAdapter
{
	private final String notAuthorizedJsonResult;
	
	/**
	 * @param objectMapper
	 * @throws JsonProcessingException
	 */
	public NotAuthorizedJsonResultInterceptor(ObjectMapper objectMapper) throws JsonProcessingException
	{
		notAuthorizedJsonResult = objectMapper.writeValueAsString(new JsonResult<String, Void>(CommonCodeResults.NOT_AUTHORIZED, null));
	}
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception
	{
		Subject subject = SecurityUtils.getSubject();
		
		if(handler instanceof HandlerMethod)
		{
			HandlerMethod handlerMethod = (HandlerMethod)handler;
			
			if(!(handlerMethod.hasMethodAnnotation(RequiresAuthentication.class) || 
				 handlerMethod.hasMethodAnnotation(RequiresRoles.class)))
			{
				return true;
			}
			
			if(!subject.isAuthenticated() && handlerMethod.hasMethodAnnotation(ResponseBody.class))
			{
				response.setStatus(HttpServletResponse.SC_FORBIDDEN);
				response.getWriter().write(notAuthorizedJsonResult);
				response.getWriter().flush();
				
				return false;
			}
		}
		
		return true;
	}
}
