package rnojiri.lib.shiro.realm;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import rnojiri.lib.interfaces.IMultiId;

/**
 * The mapped roles used by Shiro and the raw roles returned by the LDAP.
 * 
 * @author rnojiri
 */
public class LdapUser implements IMultiId<String, String>, Serializable
{
	private static final long serialVersionUID = 1669394167938129494L;

	private String name;
	
	private String login;
	
	private String mail;
	
	private Set<String> shiroRoles;
	
	private Set<String> ldapRoles;
	
	public LdapUser()
	{
		this.shiroRoles = new HashSet<>();
		this.ldapRoles = new HashSet<>();
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * @return the login
	 */
	public String getLogin()
	{
		return login;
	}

	/**
	 * @param login the login to set
	 */
	public void setLogin(String login)
	{
		this.login = login;
	}

	/**
	 * @return the mail
	 */
	public String getMail()
	{
		return mail;
	}

	/**
	 * @param mail the mail to set
	 */
	public void setMail(String mail)
	{
		this.mail = mail;
	}

	/**
	 * @return the shiroRoles
	 */
	public Set<String> getShiroRoles()
	{
		return shiroRoles;
	}

	/**
	 * @param shiroRoles the shiroRoles to set
	 */
	public void setShiroRoles(Set<String> shiroRoles)
	{
		this.shiroRoles = shiroRoles;
	}

	/**
	 * @return the ldapRoles
	 */
	public Set<String> getLdapRoles()
	{
		return ldapRoles;
	}

	/**
	 * @param ldapRoles the ldapRoles to set
	 */
	public void setLdapRoles(Set<String> ldapRoles)
	{
		this.ldapRoles = ldapRoles;
	}
	
	/**
	 * Adds one more Shiro role.
	 * 
	 * @param role
	 */
	public void addShiroRole(String role)
	{
		this.shiroRoles.add(role);
	}
	
	/**
	 * Adds one more LDAP role.
	 * 
	 * @param role
	 */
	public void addLdapRole(String role)
	{
		this.ldapRoles.add(role);
	}

	@Override
	public String getInternalId()
	{
		return login;
	}

	@Override
	public String getExternalId()
	{
		return login;
	}
}