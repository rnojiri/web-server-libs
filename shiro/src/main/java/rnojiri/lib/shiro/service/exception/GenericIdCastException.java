package rnojiri.lib.shiro.service.exception;

/**
 * Raised when an ID could not be casted.
 * 
 * @author rnojiri
 */
public class GenericIdCastException extends Exception
{
	private static final long serialVersionUID = 3608569002211122L;

	/**
	 * @param id
	 */
	public GenericIdCastException(Object id)
	{
		super("Object " + id + " cannot be casted.");
	}
}
