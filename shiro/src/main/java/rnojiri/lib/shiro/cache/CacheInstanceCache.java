package rnojiri.lib.shiro.cache;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;

import rnojiri.lib.cache.CacheInstance;

/**
 * Memcached implementation for the shiro cache.
 * 
 * V must be {@link Serializable}
 * 
 * @author rnojiri
 */
public class CacheInstanceCache<K, V> implements Cache<K, V>
{
	private int ttl;
	
	private CacheInstance cacheInstance;
	
	/**
	 * @param cacheInstance
	 * @param ttl
	 */
	public CacheInstanceCache(CacheInstance cacheInstance, int ttl)
	{
		this.cacheInstance = cacheInstance;
		this.ttl = ttl;
	}

	@Override
	public V get(K key) throws CacheException
	{
		try
		{
			return cacheInstance.get(String.valueOf(key), ttl);
		}
		catch(Exception e)
		{
			throw new CacheException(e);
		}
	}

	@Override
	public V put(K key, V value) throws CacheException
	{	
		try
		{
			String strKey = String.valueOf(key);
			
			V old = cacheInstance.get(strKey);
			
			cacheInstance.set(strKey, ttl, value);
			
			return old;
		}
		catch(Exception e)
		{
			throw new CacheException(e);
		}
	}

	@Override
	public V remove(K key) throws CacheException
	{
		try
		{
			String strKey = String.valueOf(key);
			
			V old = cacheInstance.get(strKey);
			
			cacheInstance.remove(strKey);
			
			return old;
		}
		catch(Exception e)
		{
			throw new CacheException(e);
		}
	}

	@Override
	public void clear() throws CacheException
	{
		try
		{
			cacheInstance.clear();
		}
		catch(Exception e)
		{
			throw new CacheException(e);
		}
	}

	@Override
	public int size()
	{
		return cacheInstance.size();
	}

	@Override
	public Set<K> keys()
	{
		throw new RuntimeException("Not implemented...");
	}

	@Override
	public Collection<V> values()
	{
		throw new RuntimeException("Not implemented...");
	}
}
