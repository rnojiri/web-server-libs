package rnojiri.lib.shiro.config;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.UnauthenticatedException;
import org.apache.shiro.authz.UnauthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Handles shiro specific exceptions.
 * 
 * @author rnojiri
 */
@ControllerAdvice
public class ShiroExceptionHandler
{
	private static final Logger LOGGER = LoggerFactory.getLogger(ShiroExceptionHandler.class);
	
	@Autowired
	private String loginUrl;
	
	/**
	 * Handler for the UnauthenticatedException. Sends user to the login page.
	 * 
	 * @param response
	 * @throws IOException
	 */
	@ExceptionHandler(value = {UnauthenticatedException.class, UnauthorizedException.class})
	public void handleUnauthenticatedException(HttpServletResponse response) throws IOException
	{
		if(LOGGER.isDebugEnabled())
		{
			LOGGER.debug("User has no rights to access this section, redirecting to login page...");
		}
		
		response.sendRedirect(loginUrl);
	}
}
