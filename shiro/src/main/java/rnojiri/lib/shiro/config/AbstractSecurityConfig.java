package rnojiri.lib.shiro.config;

import java.util.Map;

import javax.servlet.Filter;
import javax.sql.DataSource;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.credential.PasswordMatcher;
import org.apache.shiro.authc.credential.PasswordService;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.realm.jdbc.JdbcRealm;
import org.apache.shiro.session.mgt.eis.EnterpriseCacheSessionDAO;
import org.apache.shiro.session.mgt.eis.SessionDAO;
import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.filter.authz.RolesAuthorizationFilter;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.servlet.Cookie;
import org.apache.shiro.web.servlet.SimpleCookie;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import rnojiri.lib.cache.CacheInstance;
import rnojiri.lib.log.BeanLogger;
import rnojiri.lib.shiro.cache.CacheInstanceCacheManager;
import rnojiri.lib.shiro.filter.FilterConfig;
import rnojiri.lib.shiro.filter.FilterConfigList;
import rnojiri.lib.shiro.realm.DynamicRoleActiveDirectoryRealm;
import rnojiri.lib.shiro.realm.MockedRealm;
import rnojiri.lib.shiro.spring.BCryptPasswordService;
import rnojiri.lib.util.PropertiesManager;
import rnojiri.lib.util.PropertiesReader;
import rnojiri.lib.util.UnsupportedTypeException;

/**
 * Configures Shiro integration.
 * 
 * @author rnojiri
 */
@Order(3)
@Configuration
@Component
@DependsOn({"dataSource", "cacheManager"})
public abstract class AbstractSecurityConfig
{
	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractSecurityConfig.class);
	
	private BCryptPasswordService bCryptPasswordService;

	private SecurityManager securityManager;
	
	private Realm realm;

	public AbstractSecurityConfig()
	{
		bCryptPasswordService = new BCryptPasswordService();
	}
	
	/**
	 * Returns the cache manager.
	 * 
	 * @return CacheManager
	 * @throws UnsupportedTypeException
	 */
	@Bean(name = "cacheManager")
	public CacheManager cacheManager(CacheInstance cacheInstance) throws UnsupportedTypeException
	{
		PropertiesReader properties = PropertiesManager.getInstance().getMainProperties();
		
		return new CacheInstanceCacheManager(cacheInstance, properties.getCastedProperty(Integer.class, "cache.object.ttl"));
	}

	/**
	 * Creates the password service (bcrypt).
	 * 
	 * @return PasswordService
	 */
	@Bean
	public PasswordService passwordService()
	{
		BeanLogger.creating(PasswordService.class);

		return bCryptPasswordService;
	}

	/**
	 * Creates the JDBC realm instance.
	 * 
	 * @param cacheManager
	 * @param dataSource
	 * @param properties
	 * @return Realm
	 * @throws UnsupportedTypeException 
	 */
	private Realm createJdbcRealm(CacheManager cacheManager, DataSource dataSource, PropertiesReader properties) throws UnsupportedTypeException
	{
		PasswordMatcher passwordMatcher = new PasswordMatcher();
		passwordMatcher.setPasswordService(bCryptPasswordService);

		JdbcRealm jdbcRealm = new JdbcRealm();
		jdbcRealm.setDataSource(dataSource);
		jdbcRealm.setPermissionsLookupEnabled(false);
		jdbcRealm.setCredentialsMatcher(passwordMatcher);
		jdbcRealm.setAuthenticationQuery(properties.getProperty("jdbc.authentication.query"));
		jdbcRealm.setUserRolesQuery(properties.getProperty("jdbc.roles.query"));
		jdbcRealm.setCacheManager(cacheManager);
		jdbcRealm.setCachingEnabled(true);
		
		LOGGER.info("JDBC Realm was created!");

		return jdbcRealm;
	}
	
	/**
	 * Creates a LDAP realm instance.
	 * 
	 * @param cacheManager
	 * @param dataSource
	 * @param properties
	 * @return Realm
	 * @throws UnsupportedTypeException 
	 */
	private Realm createLdapReam(CacheManager cacheManager, DataSource dataSource, PropertiesReader properties) throws UnsupportedTypeException
	{
		final long reloadTime = properties.getCastedProperty(Long.class, "ldap.jdbc.reload.time");
		final String jdbcQuery = properties.getProperty("ldap.jdbc.roles.query");
		final String ldapFilter = properties.getProperty("ldap.filter");
		final String roleRegex = properties.getProperty("ldap.role.extraction.regex");
		
		DynamicRoleActiveDirectoryRealm activeDirectoryRealm = new DynamicRoleActiveDirectoryRealm(reloadTime, jdbcQuery, ldapFilter, roleRegex, cacheManager, dataSource);
		activeDirectoryRealm.setSystemUsername(properties.getProperty("ldap.system.user"));
		activeDirectoryRealm.setSystemPassword(properties.getProperty("ldap.system.password"));
		activeDirectoryRealm.setUrl(properties.getProperty("ldap.url"));
		activeDirectoryRealm.setSearchBase(properties.getProperty("ldap.base.dn"));
		activeDirectoryRealm.setCachingEnabled(true);
		
		LOGGER.info("LDAP Realm was created!");
		
		return activeDirectoryRealm;
	}
	
	/**
	 * Creates the mocked Realm.
	 * 
	 * @return Realm
	 */
	private Realm createMockedRealm()
	{
		LOGGER.warn("MOCKED Realm was created!");
		
		return new MockedRealm();
	}
	
	/**
	 * Creates the realm instance.
	 * 
	 * @param cacheManager
	 * @param dataSource
	 * @return Realm
	 * @throws UnsupportedTypeException
	 */
	@Bean
	public Realm realm(CacheManager cacheManager, DataSource dataSource) throws UnsupportedTypeException
	{
		if(realm != null) return realm;
		
		PropertiesReader properties = PropertiesManager.getInstance().getMainProperties();
		
		final String authRealmType = properties.getProperty("auth.realm.type"); 
		
		if("jdbc".equals(authRealmType))
		{
			realm = createJdbcRealm(cacheManager, dataSource, properties);
		} 
		else if("ldap".equals(authRealmType))
		{
			realm = createLdapReam(cacheManager, dataSource, properties);
		}
		else
		{
			realm = createMockedRealm();
		}
		
		return realm;
	}
	
	/**
	 * Creates the session manager.
	 * 
	 * @param cacheManager
	 * @return DefaultWebSessionManager
	 * @throws UnsupportedTypeException
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @throws ClassNotFoundException 
	 */
	private DefaultWebSessionManager createSessionManager(CacheManager cacheManager) throws UnsupportedTypeException, InstantiationException, IllegalAccessException, ClassNotFoundException
	{
		PropertiesReader properties = PropertiesManager.getInstance().getMainProperties();
		
		String cookieDomain = properties.getProperty("session.cookie.domain");
		
		Cookie cookie = new SimpleCookie();
		cookie.setName(properties.getProperty("session.cookie.name"));
		final Integer cookieMaxAgeSeconds = properties.getCastedProperty(Integer.class, "session.cookie.max.age");
		cookie.setMaxAge(cookieMaxAgeSeconds);
		cookie.setPath("/");
		
		if(cookieDomain != null)
		{
			cookie.setDomain(cookieDomain);
		}
		
		DefaultWebSessionManager sessionManager = new DefaultWebSessionManager();
		// Shiro's global timeout is the same as the cookie max age
		sessionManager.setGlobalSessionTimeout(cookieMaxAgeSeconds * 1000);
		sessionManager.setSessionIdCookieEnabled(true);
		sessionManager.setSessionIdCookie(cookie);
		sessionManager.setSessionIdUrlRewritingEnabled(false);
		sessionManager.setCacheManager(cacheManager);
		// SessionDAO is set according to properties in order to allow use another one than MemorySessionDAO (default)
		// (MemorySessionDAO doesn't allow share session between JVM using some backend, like database or memcached)
		@SuppressWarnings("unchecked")
		Class<SessionDAO> clazzSessionDAO = (Class<SessionDAO>) Class.forName(properties.getProperty("session.manager.dao"));
		sessionManager.setSessionDAO(clazzSessionDAO.newInstance());
		
		return sessionManager;
	}

	@Bean(name = "securityManager")
	public SecurityManager securityManager(CacheManager cacheManager, DataSource dataSource) throws UnsupportedTypeException, InstantiationException, IllegalAccessException, ClassNotFoundException
	{
		BeanLogger.creating(DefaultWebSecurityManager.class);

		if(securityManager != null) return securityManager;
		
		securityManager = new DefaultWebSecurityManager();
		
		DefaultWebSecurityManager casted = ((DefaultWebSecurityManager)securityManager); 
		casted.setRealm(realm(cacheManager, dataSource));
		casted.setSessionManager(createSessionManager(cacheManager));
		casted.setCacheManager(cacheManager);
		
		SecurityUtils.setSecurityManager(securityManager);
		
		return securityManager;
	}
	
	/**
	 * Creates the custom authorization filter to handle roles.
	 * 
	 * @return Filter
	 */
	private Filter rolesAuthorizationFilter()
	{
		PropertiesReader properties = PropertiesManager.getInstance().getMainProperties();
		
		RolesAuthorizationFilter rolesAuthorizationFilter = new RolesAuthorizationFilter();
		rolesAuthorizationFilter.setEnabled(true);
		rolesAuthorizationFilter.setLoginUrl(properties.getProperty("login.url"));
		rolesAuthorizationFilter.setName("rolesAuthorizationFilter");
		rolesAuthorizationFilter.setUnauthorizedUrl(properties.getProperty("unauthorized.url"));
		
		return rolesAuthorizationFilter;
	}

	@Bean(name = "shiroFilter")
	public ShiroFilterFactoryBean shiroFilter(CacheManager cacheManager, DataSource dataSource) throws UnsupportedTypeException, InstantiationException, IllegalAccessException, ClassNotFoundException
	{
		PropertiesReader properties = PropertiesManager.getInstance().getMainProperties();
		
		ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
		shiroFilterFactoryBean.setSecurityManager(securityManager(cacheManager, dataSource));
		shiroFilterFactoryBean.setLoginUrl(properties.getProperty("login.url"));
		shiroFilterFactoryBean.setSuccessUrl(properties.getProperty("login.success.url"));
		shiroFilterFactoryBean.setUnauthorizedUrl(properties.getProperty("unauthorized.url"));
		
		Map<String, Filter> filterMap = shiroFilterFactoryBean.getFilters();
		
		filterMap.put("rolesAuthorizationFilter", rolesAuthorizationFilter());
		
		Map<String, String> filterChainDefinitionMap = shiroFilterFactoryBean.getFilterChainDefinitionMap();
		
		FilterConfigList filterConfigList = getFilters();
		
		if(filterConfigList != null && filterConfigList.size() > 0)
		{
			for(FilterConfig filterConfig : filterConfigList)
			{
				filterMap.put(filterConfig.getName(), filterConfig.getFilter());
				filterChainDefinitionMap.put(filterConfig.getContext(), filterConfig.getName());
			}
		}
		
		return shiroFilterFactoryBean;
	}

	@Bean
	public LifecycleBeanPostProcessor lifecycleBeanPostProcessor()
	{
		BeanLogger.creating(LifecycleBeanPostProcessor.class);

		return new LifecycleBeanPostProcessor();
	}


	@Bean
	@DependsOn("lifecycleBeanPostProcessor")
	public DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator()
	{
		BeanLogger.creating(DefaultAdvisorAutoProxyCreator.class);

		return new DefaultAdvisorAutoProxyCreator();
	}

	@Bean
	public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(CacheManager cacheManager, DataSource dataSource) throws UnsupportedTypeException, InstantiationException, IllegalAccessException, ClassNotFoundException
	{
		BeanLogger.creating(AuthorizationAttributeSourceAdvisor.class);

		AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
		authorizationAttributeSourceAdvisor.setSecurityManager(securityManager(cacheManager, dataSource));

		return authorizationAttributeSourceAdvisor;
	}
	
	@Bean(name = "loginUrl")
	public String getLoginUrl()
	{
		PropertiesReader properties = PropertiesManager.getInstance().getMainProperties();
		
		return properties.getProperty("login.url");
	}
	
	/**
	 * Returns a list of custom filters.
	 * 
	 * @return FilterConfigList
	 */
	protected abstract FilterConfigList getFilters();
}
