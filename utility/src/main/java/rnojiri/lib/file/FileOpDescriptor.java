package rnojiri.lib.file;

import java.nio.file.Path;

/**
 * Returned as a file operation result.
 * 
 * @author rnojiri
 */
public class FileOpDescriptor
{
	public final Path path;
	
	public final byte bytes[];
	
	public final long numBytes;
	
	public final byte[] md5sum;

	/**
	 * @param path
	 * @param bytes
	 * @param md5sum
	 */
	public FileOpDescriptor(Path path, byte bytes[], byte[] md5sum)
	{
		this.path = path;
		this.bytes = bytes;
		this.numBytes = bytes.length;
		this.md5sum = md5sum;
	}
}
