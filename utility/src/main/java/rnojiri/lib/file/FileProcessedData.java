package rnojiri.lib.file;

/**
 * The processed data from the input stream.
 * 
 * @author rnojiri
 */
public class FileProcessedData
{
	public final byte bytes[];
	
	public final byte md5sum[];

	/**
	 * @param bytes
	 * @param md5sum
	 */
	public FileProcessedData(byte[] bytes, byte[] md5sum)
	{
		this.bytes = bytes;
		this.md5sum = md5sum;
	}
}
