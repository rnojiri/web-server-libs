package rnojiri.lib.file;

/**
 * Raised when an invalid depth is specified.
 * 
 * @author rnojiri
 */
public class InvalidDepthException extends Exception
{
	private static final long serialVersionUID = 2626813683035880274L;

	/**
	 * @param depth
	 * @param max
	 */
	public InvalidDepthException(int depth, int max)
	{
		super("The depth " + depth + " must be multiple of " + max + " and not zero.");
	}
}
