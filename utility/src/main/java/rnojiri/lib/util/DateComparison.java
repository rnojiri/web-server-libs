package rnojiri.lib.util;

import java.util.Date;

/**
 * Compares dates.
 * 
 * @author rnojiri
 */
public class DateComparison
{
	public static final long ONE_SEC_IN_MILLISECONDS = 1000L;
	
	/**
	 * Returns a date comparison using a acceptable threshold in milliseconds.
	 * 
	 * @param date1
	 * @param date2
	 * @param threshold
	 * @return boolean
	 */
	public static boolean equalsWithAcceptableThreshold(Date date1, Date date2, long threshold)
	{
		if(date1 == null && date2 == null)
		{
			return true;
		}
		else if((date1 == null && date2 != null) || date1 != null && date2 == null)
		{
			return false;
		}
		else
		{
			return (date1.getTime() - date2.getTime()) <=  threshold;
		}
	}
}
