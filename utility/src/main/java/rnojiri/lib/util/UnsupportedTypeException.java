package rnojiri.lib.util;

/**
 * Type is not supported exception.
 * 
 * @author rnojiri
 */
public class UnsupportedTypeException extends Exception
{
	private static final long serialVersionUID = -1645765409777216376L;

	/**
	 * @param clazzType
	 */
	public UnsupportedTypeException(Class<?> classType)
	{
		super("Class \"" + classType.getCanonicalName() + "\" is not supported in validation.");
	}
}
