package rnojiri.lib.util;

import java.io.IOException;
import java.util.HashMap;

/**
 * Manages all loaded properties in a singleton.
 * 
 * @author rnojiri
 */
public class PropertiesManager
{
	private static PropertiesManager instance;
	
	private HashMap<String, PropertiesReader> propertiesFilesMap;
	
	private PropertiesReader mainProperties;
	
	private PropertiesManager()
	{
		propertiesFilesMap = new HashMap<>();
	}

	/**
	 * @return the instance
	 */
	public static PropertiesManager getInstance()
	{
		if(instance == null)
		{
			instance = new PropertiesManager();
		}
		
		return instance;
	}
	
	/**
	 * Returns a properties reader by name.
	 * 
	 * @param name
	 * @return PropertiesReader
	 */
	public PropertiesReader getProperty(String name)
	{
		return propertiesFilesMap.get(name);
	}
	
	/**
	 * Loads a properties file and gives it a name.
	 * 
	 * @param name
	 * @param file
	 * @param setAsMain
	 * @return PropertiesReader
	 * @throws IOException
	 */
	public PropertiesReader loadProperties(String name, String file, boolean setAsMain) throws IOException
	{
		PropertiesReader propertiesReader = new PropertiesReader(file);
		
		propertiesFilesMap.put(name, propertiesReader);
		
		if(setAsMain)
		{
			mainProperties = propertiesReader;
		}
		
		return propertiesReader;
	}
	
	/**
	 * Loads a properties file and gives it the file name.
	 * 
	 * @param file
	 * @param setAsMain
	 * @return PropertiesReader
	 * @throws IOException
	 */
	public PropertiesReader loadProperties(String file, boolean setAsMain) throws IOException
	{
		return loadProperties(file, file, setAsMain);
	}

	/**
	 * @return the mainProperties
	 */
	public PropertiesReader getMainProperties()
	{
		return mainProperties;
	}
}
