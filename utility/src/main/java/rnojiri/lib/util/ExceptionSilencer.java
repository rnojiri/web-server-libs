package rnojiri.lib.util;

import java.io.Closeable;
import java.io.IOException;

/**
 * Handles exceptions.
 * 
 * @author rnojiri
 */
public class ExceptionSilencer
{
	/**
	 * Closes in silence.
	 * 
	 * @param closeable
	 */
	public static void close(Closeable closeable)
	{
		try
		{
			if(closeable != null)
			{
				closeable.close();
			}
		}
		catch(IOException e)
		{
			;
		}
	}
}
