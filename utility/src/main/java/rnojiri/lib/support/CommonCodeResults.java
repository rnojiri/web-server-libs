package rnojiri.lib.support;

/**
 * Some common result codes.
 * 
 * @author rnojiri
 */
public class CommonCodeResults
{
	public static final String SUCCESS = "success";
	
	public static final String NOT_FOUND = "not_found";
	
	public static final String EMPTY = "empty";
	
	public static final String FAILURE = "failure";
	
	public static final String DUPLICATED = "duplicated";
	
	public static final String VALIDATION_FAILURE = "validation_failure";
	
	public static final String NO_ACTION = "no_action";
	
	public static final String NOT_AUTHORIZED = "not_authorized";
	
	protected CommonCodeResults()
	{
		;
	}
}
