package rnojiri.lib.support;

/**
 * Base result json.
 * 
 * @author rnojiri
 *
 * @param <C>
 * @param <D>
 */
public class JsonResult<C, D>
{
	private C resultCode;
	
	private D data;
	
	public JsonResult()
	{
		;
	}
	
	/**
	 * @param resultCode
	 */
	public JsonResult(C resultCode)
	{
		this(resultCode, null);
	}

	/**
	 * @param resultCode
	 * @param data
	 */
	public JsonResult(C resultCode, D data)
	{
		this.resultCode = resultCode;
		this.data = data;
	}

	/**
	 * @return the resultCode
	 */
	public C getResultCode()
	{
		return resultCode;
	}

	/**
	 * @param resultCode the resultCode to set
	 */
	public void setResultCode(C resultCode)
	{
		this.resultCode = resultCode;
	}

	/**
	 * @return the data
	 */
	public D getData()
	{
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(D data)
	{
		this.data = data;
	}
	
	/**
	 * @param resultCode
	 * @param data
	 */
	public void setAll(C resultCode, D data)
	{
		this.resultCode = resultCode;
		this.data = data;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "ResultJson [resultCode=" + resultCode + ", data=" + data + "]";
	}
}
