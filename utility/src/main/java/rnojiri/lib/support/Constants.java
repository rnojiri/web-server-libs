package rnojiri.lib.support;

/**
 * Common constants.
 * 
 * @author rnojiri
 */
public class Constants
{	
	public static final String EMPTY = "";
	
	public static final String WHITE_SPACE = " ";
	
	public static final String PIPE = "|";
	
	public static final String EQUALS = "=";
	
	public static final String DIR_SEPARATOR = "/";
	
	public static final String LINE_BREAK = "\n";
	
	public static final String UTF_8 = "UTF-8";
	
	public static final String BRAZILIAN_DATE_FORMAT = "dd/MM/yyyy";
	
	public static final String BRAZILIAN_DATE_FORMAT_AND_TIME = BRAZILIAN_DATE_FORMAT + " HH:mm:ss";
	
	public static final int BUFFER_SIZE_8 = 8;
	
	public static final int BUFFER_SIZE_16 = 16;
	
	public static final int BUFFER_SIZE_32 = 32;
	
	public static final int BUFFER_SIZE_64 = 64;
	
	public static final int BUFFER_SIZE_128 = 128;
	
	public static final int BUFFER_SIZE_256 = 256;
	
	public static final int BUFFER_SIZE_512 = 512;
	
	public static final int BUFFER_SIZE_1024 = 1024;
	
	public static final int BUFFER_SIZE_2048 = 2048;
	
	public static final int BUFFER_SIZE_4096 = 4096;
		
	private Constants()
	{
		;
	}
}
