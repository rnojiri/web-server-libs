package rnojiri.lib.cache;

import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import rnojiri.lib.support.Constants;

/**
 * A generic cache manager.
 * 
 * @author rnojiri
 */
public class CacheManager
{
	private static final Logger LOGGER = LoggerFactory.getLogger(CacheManager.class);
	
	private final CacheInstance cacheInstance;
	
	public CacheManager(CacheInstance cacheInstance)
	{
		this.cacheInstance = cacheInstance;
	}
	
	/**
	 * Builds the cache key.
	 * 
	 * @param paths
	 * @return String
	 */
	private String buildCacheKey(String ... paths)
	{
		StringBuilder keyBuilder = new StringBuilder(Constants.BUFFER_SIZE_128);
		
		if(paths != null && paths.length > 0)
		{	
			for(int i=0; i<paths.length; i++)
			{
				keyBuilder.append('/').append(paths[i]);
			}
		}
		else
		{
			keyBuilder.append('/');
		}
		
		return keyBuilder.toString();
	}
	
	/**
	 * Returns a cache object with no TTL.
	 * 
	 * @param propertyName
	 * @return Object
	 * @throws Exception 
	 */
	public <T> T get(String ... paths) throws Exception
	{
		return get(null, paths);
	}
	
	/**
	 * Returns a cache object and touches it using the specified TTL.
	 * 
	 * @param propertyName
	 * @return Object
	 * @throws Exception 
	 */
	public <T> T get(Integer touchTTL, String ... paths) throws Exception
	{
		String key = buildCacheKey(paths);
		
		@SuppressWarnings("unchecked")
		T object = (T)(touchTTL != null ? cacheInstance.get(key, touchTTL) : cacheInstance.get(key));
		
		if(LOGGER.isDebugEnabled())
		{
			LOGGER.debug("Value \"" + Objects.toString(object) + "\" found under key \"" + key + "\".");
		}
		
		return object;
	}
	
	/**
	 * Adds/Replaces an object in the cache.
	 * 
	 * @param sessionId
	 * @param value
	 * @param root
	 * @param paths
	 * @throws Exception 
	 */
	public void setCache(Object value, int ttl, String ... paths) throws Exception
	{
		String key = buildCacheKey(paths);
		
		cacheInstance.set(key, ttl, value);
		
		if(LOGGER.isDebugEnabled())
		{
			LOGGER.debug("Value \"" + Objects.toString(value) + "\" was set under key \"" + key + "\".");
		}
	}
	
	/**
	 * Removes an object by its key.
	 * 
	 * @param root
	 * @param paths
	 * @throws Exception 
	 */
	public boolean remove(String ... paths) throws Exception
	{
		String key = buildCacheKey(paths);
		
		boolean result = cacheInstance.remove(key);
		
		if(LOGGER.isDebugEnabled())
		{
			LOGGER.debug("Removed cache value under key \"" + key + "\".");
		}
		
		return result;
	}
}
