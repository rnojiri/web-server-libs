package rnojiri.lib.cache;


/**
 * A generic cache instance implementation.
 * 
 * @author rnojiri
 */
public interface CacheInstance
{
	/**
	 * Returns an object from the cache.
	 * 
	 * @param key
	 * @return T
	 * @throws Exception
	 */
	<T> T get(String key) throws Exception;
	
	/**
	 * Returns an object from the cache and touches it using the specified TTL (seconds).
	 * 
	 * @param key
	 * @return T
	 * @throws Exception
	 */
	<T> T get(String key, int ttl) throws Exception;
	
	/**
	 * Adds/Replaces an object in the cache.
	 * 
	 * @param key
	 * @param ttl
	 * @param object
	 * @throws Exception
	 */
	void set(String key, int ttl, Object object) throws Exception;
	
	/**
	 * Removes an object by its key.
	 * 
	 * @param key
	 * @return boolean
	 * @throws Exception
	 */
	boolean remove(String key) throws Exception;
	
	/**
	 * Shuts down this client.
	 */
	void shutdown();
	
	/**
	 * Clear the cache.
	 */
	void clear() throws Exception;
	
	/**
	 * Returns the size.
	 * 
	 * @return int
	 */
	int size();
}
