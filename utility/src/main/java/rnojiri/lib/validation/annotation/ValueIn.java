package rnojiri.lib.validation.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * A field with a set of values allowed.
 * 
 * @author rnojiri
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface ValueIn
{
	/**
	 * A list with allowed values.
	 * 
	 * @return String[]
	 */
	String[] value();
}
