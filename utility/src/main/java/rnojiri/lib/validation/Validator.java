package rnojiri.lib.validation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import rnojiri.lib.support.Constants;
import rnojiri.lib.util.ReflectionUtil;
import rnojiri.lib.util.UnsupportedTypeException;
import rnojiri.lib.validation.annotation.MaximumSize;
import rnojiri.lib.validation.annotation.MinimumSize;
import rnojiri.lib.validation.annotation.OptionalParam;
import rnojiri.lib.validation.annotation.RegexpValidation;
import rnojiri.lib.validation.annotation.RequiredParam;
import rnojiri.lib.validation.annotation.RequiredParamIf;
import rnojiri.lib.validation.annotation.ValueIn;

/**
 * The main validation class using annotations.
 * 
 * @author rnojiri
 */
public class Validator
{
	private static final Logger LOGGER = LoggerFactory.getLogger(Validator.class);

	private static final Pattern ARRAY_SPLIT_PATTERN = Pattern.compile("[ ]*,[ ]*", Pattern.CASE_INSENSITIVE);
	
	private static final Set<Method> OBJECT_METHOD_MAP;
	static
	{
		OBJECT_METHOD_MAP = new HashSet<>();
		
		for(Method method : Object.class.getMethods())
		{
			OBJECT_METHOD_MAP.add(method);
		}
	}
	
	/**
	 * Returns a map with invalid fields.
	 * 
	 * @param object
	 * @param currentSession
	 * @return Map<String, String>
	 * @throws Exception
	 */
	public static Map<String, String> verify(Validable ...object)
	{
		Map<String, String> invalidFieldsMap = new HashMap<String, String>();

		for(Validable obj : object)
		{
			Map<String, String> invalidFieldMap = new HashMap<String, String>();

			verify(obj, invalidFieldMap, false, -1);

			if (invalidFieldMap.size() == 0)
			{
				PostValidation<?> businessValidations[] = obj.getPostValidations();
				
				if (businessValidations != null && businessValidations.length > 0)
				{
					for (int i = 0; i < businessValidations.length; i++)
					{
						if (!businessValidations[i].isValid(obj))
						{
							invalidFieldMap.put(ValidationError.POST_VERIFICATION.name(), businessValidations[i].getErrorCode());
						}
					}
				}
			}
			
			if (invalidFieldMap.size() > 0)
			{				
				invalidFieldsMap.putAll(invalidFieldMap);
			}
		}

		return invalidFieldsMap;
	}
	
	/**
	 * Extracts all methods from the object.
	 * 
	 * @param object
	 * @return Collection<Method>
	 */
	private static Collection<Method> extractMethods(Object object)
	{
		Map<String, Method> methodMap = new HashMap<>();
		
		Class<?> pointer = object.getClass();
		
		do
		{
			putEligibleMethods(methodMap, pointer.getMethods());
			
			Class<?>[] interfaces = pointer.getInterfaces();
			
			if(interfaces != null && interfaces.length > 0)
			{
				for(Class<?> clazz : interfaces)
				{
					putEligibleMethods(methodMap, clazz.getMethods());
				}
			}
			
			pointer = pointer.getSuperclass();
			
		}while(!Object.class.equals(pointer));
		
		return methodMap.values();
	}

	/**
	 * Puts all eligible methods for verification.
	 * 
	 * @param methodMap
	 * @param methods
	 */
	private static void putEligibleMethods(Map<String, Method> methodMap, Method[] methods)
	{
		if(methods != null && methods.length > 0)
		{
			for(Method method : methods)
			{
				Annotation annotations[] = method.getAnnotations();
				
				if(!(method.getReturnType().isPrimitive() && void.class == method.getReturnType()) 
						&& !OBJECT_METHOD_MAP.contains(method) && annotations != null && annotations.length > 0)
				{
					methodMap.put(method.getName(), method);
				}
			}
		}
	}
	
	/**
	 * Inner validation.
	 * 
	 * @param object
	 * @param invalidFieldMap
	 * @param isList
	 * @param index
	 */
	private static void verify(Object object, Map<String, String> invalidFieldMap, boolean isList, int index)
	{
		Collection<Method> methods = extractMethods(object);
		
		if (methods == null || methods.isEmpty())
		{
			return;
		}

		for (Method method : methods)
		{
			if (Modifier.isFinal(method.getModifiers())) continue;

			String methodName = method.getName();

			Object value = getFieldValueByMethod(object, method, invalidFieldMap);

			setDefaultValue(object, invalidFieldMap, isList, index, method, methodName, value);
		}

		for (Method method : methods)
		{
			if (Modifier.isFinal(method.getModifiers())) continue;

			String methodName = method.getName();

			Object value = getFieldValueByMethod(object, method, invalidFieldMap);

			try
			{
				if (value instanceof List)
				{
					List<?> list = (List<?>) value;

					validateField(object, invalidFieldMap, isList, index, method, methodName, value);

					for (int i = 0; i < list.size(); i++)
					{
						verify(list.get(i), invalidFieldMap, true, i);
					}
				}
				else
				{
					validateField(object, invalidFieldMap, isList, index, method, methodName, value);
				}
			}
			catch (UnsupportedTypeException e)
			{
				addValidationError(methodName, ValidationError.PARAMETER_TYPE_IS_NOT_SUPPORTED, invalidFieldMap, isList, index);

				LOGGER.error("Error parsing default value.", e);
			}
		}
	}

	/**
	 * Validates a single field.
	 * 
	 * @param object
	 * @param invalidFieldMap
	 * @param isList
	 * @param index
	 * @param method
	 * @param methodName
	 * @param value
	 * @throws UnsupportedTypeException
	 */
	private static void validateField(Object object, 
	                                  Map<String, String> invalidFieldMap, 
	                                  boolean isList, 
	                                  int index, 
	                                  Method method, 
	                                  String methodName, 
	                                  Object value)
			throws UnsupportedTypeException
	{
		Annotation annotations[] = method.getDeclaredAnnotations();

		if (annotations != null && annotations.length > 0)
		{
			for (Annotation annotation : annotations)
			{
				if (annotation instanceof RequiredParam)
				{
					checkForRequiredValue(invalidFieldMap, isList, index, methodName, value);
				}
				else if (annotation instanceof RequiredParamIf)
				{
					RequiredParamIf requiredParamIf = ((RequiredParamIf) annotation);
					
					Object requiredParamValue = getFieldValue(object, requiredParamIf.parameter(), invalidFieldMap);
					
					if (requiredParamValue != null)
					{
						Object parsedValue = ReflectionUtil.parseValue(requiredParamValue.getClass(), requiredParamIf.value());
						
						if (requiredParamValue.equals(parsedValue))
						{
							checkForRequiredValue(invalidFieldMap, isList, index, methodName, value);
						}
						else
						{
							return;
						}
					}
					else if (StringUtils.isBlank(requiredParamIf.value()))
					{
						checkForRequiredValue(invalidFieldMap, isList, index, methodName, value);
					}

				}
				else if (annotation instanceof OptionalParam)
				{
					if (value == null)
					{
						OptionalParam optionalParam = ((OptionalParam) annotation);

						Class<?> fieldType = method.getReturnType();

						Object castedValue = null;

						if (!fieldType.isArray())
						{
							castedValue = ReflectionUtil.parseValue(fieldType, optionalParam.defaultValue());
						}
						else
						{
							Class<?> componentType = fieldType.getComponentType();

							String array[] = ARRAY_SPLIT_PATTERN.split(optionalParam.defaultValue());

							castedValue = Array.newInstance(componentType, array.length);

							for (int i = 0; i < array.length; i++)
							{
								Object parsedArrayItem = ReflectionUtil.parseValue(componentType, array[i]);

								if (parsedArrayItem != null)
								{
									Array.set(castedValue, i, parsedArrayItem);
								}
							}
						}

						if (castedValue != null)
						{
							setMethodValue(object, methodName, invalidFieldMap, castedValue, isList, index);
						}
					}
				}
				else if (annotation instanceof RegexpValidation)
				{
					RegexpValidation regexpValidation = (RegexpValidation) annotation;

					if (value != null && value instanceof String && StringUtils.isNotBlank((String) value)
							&& !Pattern.compile(regexpValidation.value(), Pattern.CASE_INSENSITIVE).matcher((String) value).find())
					{
						addValidationError(methodName, ValidationError.REGULAR_EXPRESSION_MISMATCH, invalidFieldMap, isList, index);
					}
				}
				else if (annotation instanceof MinimumSize)
				{
					if (value == null)
					{
						addValidationError(methodName, ValidationError.BELOW_MINIMUM_SIZE, invalidFieldMap, isList, index);
					}
					else
					{
						int minimumSize = ((MinimumSize) annotation).value();

						if (value instanceof List)
						{
							List<?> list = (List<?>) value;

							if (list.size() < minimumSize)
							{
								addValidationError(methodName, ValidationError.BELOW_MINIMUM_SIZE, invalidFieldMap, isList, index);
							}
						}
						else if (value instanceof String)
						{
							if (((String) value).length() < minimumSize)
							{
								addValidationError(methodName, ValidationError.BELOW_MINIMUM_SIZE, invalidFieldMap, isList, index);
							}
						}
						else if (value instanceof Number)
						{
							if (((Number) value).floatValue() < minimumSize)
							{
								addValidationError(methodName, ValidationError.BELOW_MINIMUM_SIZE, invalidFieldMap, isList, index);
							}
						}
					}
				}
				else if (annotation instanceof MaximumSize)
				{
					if (value != null)
					{
						int maximumSize = ((MaximumSize) annotation).value();

						if (value instanceof List)
						{
							List<?> list = (List<?>) value;

							if (list.size() > maximumSize)
							{
								addValidationError(methodName, ValidationError.ABOVE_MAXIMUM_SIZE, invalidFieldMap, isList, index);
							}
						}
						else if (value instanceof String)
						{
							if (((String) value).length() > maximumSize)
							{
								addValidationError(methodName, ValidationError.ABOVE_MAXIMUM_SIZE, invalidFieldMap, isList, index);
							}
						}
						else if (value instanceof Number)
						{
							if (((Number) value).floatValue() > maximumSize)
							{
								addValidationError(methodName, ValidationError.ABOVE_MAXIMUM_SIZE, invalidFieldMap, isList, index);
							}
						}
					}
				}
				else if (annotation instanceof ValueIn)
				{
					if (value == null || (value instanceof String && StringUtils.isBlank((String) value))) continue;

					String allowedValues[] = ((ValueIn) annotation).value();

					Class<?> fieldClass = value.getClass();

					boolean foundValue = false;

					for (int i = 0; i < allowedValues.length; i++)
					{
						if (value.equals(ReflectionUtil.parseValue(fieldClass, allowedValues[i])))
						{
							foundValue = true;

							break;
						}
					}

					if (!foundValue)
					{
						addValidationError(methodName, ValidationError.INVALID_PARAMETER_OPTION, invalidFieldMap, isList, index);
					}
				}
			}
		}
	}

	/**
	 * Sets the default value.
	 * 
	 * @param object
	 * @param invalidFieldMap
	 * @param isList
	 * @param index
	 * @param method
	 * @param methodName
	 * @param value
	 */
	private static void setDefaultValue(Object object,
										Map<String, String> invalidFieldMap,
										boolean isList,
										int index,
										Method method,
										String methodName,
										Object value)
	{
		try
		{
			Annotation annotations[] = method.getDeclaredAnnotations();

			if (annotations != null && annotations.length > 0)
			{
				for (Annotation annotation : annotations)
				{
					if (annotation instanceof OptionalParam)
					{
						if (value != null) continue;

						OptionalParam optionalParam = ((OptionalParam) annotation);

						Class<?> returnType = method.getReturnType();

						Object castedValue = null;

						if (!returnType.isArray())
						{
							castedValue = ReflectionUtil.parseValue(returnType, optionalParam.defaultValue());
						}
						else
						{
							Class<?> componentType = returnType.getComponentType();

							String array[] = ARRAY_SPLIT_PATTERN.split(optionalParam.defaultValue());

							castedValue = Array.newInstance(componentType, array.length);

							for (int i = 0; i < array.length; i++)
							{
								Object parsedArrayItem = ReflectionUtil.parseValue(componentType, array[i]);

								if (parsedArrayItem != null)
								{
									Array.set(castedValue, i, parsedArrayItem);
								}
							}
						}

						if (castedValue != null)
						{
							setMethodValue(object, methodName, invalidFieldMap, castedValue, isList, index);
						}
					}
				}
			}
		}
		catch (Exception e)
		{
			addValidationError(methodName, ValidationError.PARAMETER_TYPE_IS_NOT_SUPPORTED, invalidFieldMap, isList, index);

			LOGGER.error("Error parsing default value.", e);
		}
	}

	/**
	 * Checks if required.
	 * 
	 * @param invalidFieldMap
	 * @param isList
	 * @param index
	 * @param methodName
	 * @param value
	 * @param currentSession
	 */
	private static void checkForRequiredValue(Map<String, String> invalidFieldMap, boolean isList, int index, String methodName, Object value)
	{
		if (value == null || (value instanceof CharSequence && StringUtils.isBlank((CharSequence)value)) || (value.getClass().isArray() && Array.getLength(value) == 0))
		{
			addValidationError(methodName, ValidationError.EMPTY_REQUIRED_FIELD, invalidFieldMap, isList, index);
		}
	}

	/**
	 * Adds a new validation error.
	 * 
	 * @param methodName
	 * @param validationError
	 * @param invalidFieldMap
	 * @param isList
	 * @param index
	 */
	private static void addValidationError(final String methodName,
										   final ValidationError validationError,
										   Map<String, String> invalidFieldMap,
										   boolean isList,
										   int index)
	{
		String key = null;
		
		final String fieldName = ReflectionUtil.getFieldNameByMethod(methodName);

		if (!isList)
		{
			key = fieldName;
		}
		else
		{
			key = new StringBuilder(Constants.BUFFER_SIZE_32).append(fieldName).append('[').append(index).append(']').toString();
		}

		if (!invalidFieldMap.containsKey(key))
		{
			invalidFieldMap.put(key, validationError.name());
		}
	}

	/**
	 * Returns the field value.
	 * 
	 * @param object
	 * @param fieldName
	 * @param invalidFieldMap
	 * @return Object
	 */
	private static Object getFieldValue(Object object, String fieldName, Map<String, String> invalidFieldMap)
	{
		Object value = null;

		try
		{
			value = ReflectionUtil.getFieldValue(object, fieldName);
		}
		catch (Exception e)
		{
			if (LOGGER.isDebugEnabled())
			{
				LOGGER.debug("Error getting field value.", e);
			}

			value = null;
		}

		return value;
	}
	
	/**
	 * Returns the field value.
	 * 
	 * @param object
	 * @param method
	 * @param invalidFieldMap
	 * @return Object
	 */
	private static Object getFieldValueByMethod(Object object, Method method, Map<String, String> invalidFieldMap)
	{
		Object value = null;

		try
		{
			value = method.invoke(object);
		}
		catch (Exception e)
		{
			if (LOGGER.isDebugEnabled())
			{
				LOGGER.debug("Error getting field value.", e);
			}

			value = null;
		}

		return value;
	}

	/**
	 * Sets the field value.
	 * 
	 * @param object
	 * @param methodName
	 * @param invalidFieldMap
	 * @param value
	 * @param isList
	 * @param index
	 */
	private static void setMethodValue(Object object, String methodName, Map<String, String> invalidFieldMap, Object value, boolean isList, int index)
	{
		try
		{
			char methodNameCharArray[] = methodName.toCharArray();
			
			methodNameCharArray[0] = 's';
			
			Method method = object.getClass().getMethod(new String(methodNameCharArray), value.getClass());
			
			method.invoke(object, value);
		}
		catch (Exception e)
		{
			LOGGER.error("Error setting field value.", e);

			addValidationError(methodName, ValidationError.SET_METHOD_ERROR, invalidFieldMap, isList, index);
		}
	}

	/**
	 * Hidden.
	 */
	private Validator()
	{
		;
	}
}
