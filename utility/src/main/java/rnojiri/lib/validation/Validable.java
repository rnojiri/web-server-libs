package rnojiri.lib.validation;

/**
 * An validable class.
 * 
 * @author rnojiri
 */
public interface Validable
{
	/**
	 * Returns all business validations from form.
	 * 
	 * @return BusinessValidation<? extends Verifiable>[]
	 */
	public PostValidation<? extends Validable>[] getPostValidations();
}
