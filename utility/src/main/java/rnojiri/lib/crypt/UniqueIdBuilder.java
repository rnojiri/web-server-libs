package rnojiri.lib.crypt;

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * A class to generate unique IDs.
 * 
 * @author rnojiri
 */
public class UniqueIdBuilder
{
	/**
	 * Builds a generic ID.
	 * 
	 * @return String
	 */
	public static String generic()
	{
		return (new BigInteger(130, new SecureRandom()).toString(32));
	}
}
