package rnojiri.lib.crypt;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Builds encrypted string using AES algorithm.
 * 
 * @author rnojiri
 */
public class AesEncryptor
{
	private static Logger log = LoggerFactory.getLogger(AesEncryptor.class); 
	
	private final static String HEX = "0123456789ABCDEF";
	private final static String RANDOM_ALGORITHM = "SHA1PRNG";
	private final static String AES = "AES";
	private final static String EMPTY = "";
	private final static int KEY_SIZE = 128;
	
	private Cipher encryptCipher;
	private Cipher decryptCipher;

	/**
	 * Initializes with a constant seed.
	 * 
	 * @param seed
	 */
	public AesEncryptor(String seed)
	{
		try
		{
			byte raw[] = getRawKey(seed.getBytes());
			
			SecretKeySpec skeySpec = new SecretKeySpec(raw, AES);
			
			encryptCipher = Cipher.getInstance(AES);
			encryptCipher.init(Cipher.ENCRYPT_MODE, skeySpec);
			
			decryptCipher = Cipher.getInstance(AES);
			decryptCipher.init(Cipher.DECRYPT_MODE, skeySpec);
			
			log.info("AES ciphers were created with success!");
		}
		catch(Exception e)
		{
			log.error("Error creating ciphers.", e);
			
			throw new EncryptorException(e);
		}
	}

	/**
	 * Encrypts the text.
	 * 
	 * @param cleartext
	 * @return String
	 */
	public String encrypt(String cleartext)
	{
		try
		{
			byte[] result = encryptCipher.doFinal(cleartext.getBytes());
			
			return toHex(result);
		}
		catch(Exception e)
		{
			log.error("Error encrypting text \"" + cleartext + "\".", e);
			
			throw new EncryptorException(e);
		}
	}

	/**
	 * Decrypts the text encrypted.
	 * 
	 * @param encrypted
	 * @return String (null if not decrypted)
	 */
	public String decrypt(String encrypted)
	{
		try
		{
			byte[] encryptedInBytes = toByte(encrypted);
			byte[] result = decryptCipher.doFinal(encryptedInBytes);
			
			return new String(result);
		}
		catch(Exception e)
		{
			log.error("Error decrypting \"" + encrypted + "\".", e);
			
			return null;
		}
	}

	/**
	 * Generates a raw key using the given seed.
	 * 
	 * @param seed
	 * @return byte[]
	 * @throws NoSuchAlgorithmException 
	 */
	private byte[] getRawKey(byte[] seed) throws NoSuchAlgorithmException
	{
		KeyGenerator kgen = KeyGenerator.getInstance(AES);
		
		SecureRandom sr = SecureRandom.getInstance(RANDOM_ALGORITHM);
		sr.setSeed(seed);
		
		kgen.init(KEY_SIZE, sr); // 192 and 256 bits may not be available
		
		SecretKey skey = kgen.generateKey();
		byte[] raw = skey.getEncoded();
		
		return raw;
	}
	
	/**
	 * Converts a hex String to byte array.
	 * 
	 * @param hexString
	 * @return byte[]
	 */
	public byte[] toByte(String hexString)
	{
		int len = hexString.length();
		byte[] data = new byte[len / 2];
		
		for (int i=0; i<len; i+=2)
		{
			data[i/2] = (byte)((Character.digit(hexString.charAt(i), 16) << 4) + Character.digit(hexString.charAt(i + 1), 16));
		}
		
		return data;
	}

	/**
	 * Converts a byte array to hex.
	 * 
	 * @param buf
	 * @return byte[] buf
	 */
	private String toHex(byte[] buf)
	{
		if (buf == null)
		{
			return EMPTY;
		}
		
		StringBuffer result = new StringBuffer(2 * buf.length);
		
		for (int i = 0; i < buf.length; i++)
		{
			appendHex(result, buf[i]);
		}
		
		return result.toString();
	}

	/**
	 * Appends the correct hex.
	 * 
	 * @param sb
	 * @param b
	 */
	private void appendHex(StringBuffer sb, byte b)
	{
		sb.append(HEX.charAt((b >> 4) & 0x0f)).append(HEX.charAt(b & 0x0f));
	}
}
