package rnojiri.lib.collection;

import java.util.LinkedHashMap;

import rnojiri.lib.support.KeyValue;

public class PreBuiltLinkedHashMap<K, V> extends LinkedHashMap<K, V>
{
	private static final long serialVersionUID = -7877246355487643700L;
	
	/**
	 * Adds all keys and values to the map.
	 * 
	 * @param keys
	 * @param values
	 */
	public PreBuiltLinkedHashMap(KeyValue<K, V> items[])
	{
		if(items != null && items.length > 0)
		{
			for(int i=0; i<items.length; i++)
			{
				put(items[i].getKey(), items[i].getValue());
			}
		}
	}
}
