package rnojiri.lib.collection;

import java.util.HashSet;

/**
 * A hash set with parameterized constructor to insert data on initialization. 
 * 
 * @author rnojiri
 *
 * @param <E>
 */
public class PreBuiltHashSet<E> extends HashSet<E>
{
	private static final long serialVersionUID = -6897743766227195121L;

	@SafeVarargs
	public PreBuiltHashSet(E ... items)
	{
		super();
		
		if(items != null && items.length > 0)
		{
			for(E item : items)
			{
				add(item);
			}
		}
	}
	
	/**
	 * Builds a new PreBuiltHashSet<E> using the given items. Returns null if none.  
	 * 
	 * @param items
	 * @return PreBuiltHashSet<E>
	 */
	@SuppressWarnings("unchecked")
	public static <E> PreBuiltHashSet<E> build(E ... items)
	{
		if(items == null || items.length == 0) return null; 
		
		return new PreBuiltHashSet<>(items);
	}
}
