package rnojiri.lib.tcp;

/**
 * Raised when an issued command is not found in a protocol.
 * 
 * @author rnojiri
 */
public class UnknownProtocolCommandException extends Exception
{
	private static final long serialVersionUID = 2068145625029004250L;

	/**
	 * @param command
	 */
	public UnknownProtocolCommandException(String command)
	{
		super("Protocol command \"" + command + "\" is unknown.");
	}
}
