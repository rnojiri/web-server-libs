package rnojiri.lib.tcp;


/**
 * Executes all custom protocol commands sent to the server.
 * 
 * @author rnojiri
 */
public interface CommandExecutor
{
	/**
	 * Executes a custom command.
	 * 
	 * @param command
	 * @return boolean
	 * @throws Exception
	 */
	void execute(Command command) throws Exception;
}
