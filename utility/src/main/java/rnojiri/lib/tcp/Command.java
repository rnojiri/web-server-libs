package rnojiri.lib.tcp;

/**
 * A protocol command.
 * 
 * @author rnojiri
 */
public class Command
{
	public final String command;
	
	public final Object[] parameters;

	/**
	 * @param command
	 * @param parameters
	 */
	public Command(String command, Object ... parameters)
	{
		super();
		this.command = command;
		this.parameters = parameters;
	}
}
