package rnojiri.lib.tcp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A thread to handle the client requisition.
 * 
 * @author rnojiri
 */
public class SocketHandlerThread implements Runnable
{
	private static final Logger LOGGER = LoggerFactory.getLogger(SocketHandlerThread.class);

	private Socket socket;

	private final CommandExecutor commandExecutor;
	
	private final Protocol protocol;
	
	private final String okResponse;
	
	private final String endResponse;
	
	private final String waitingResponse;
	
	private final String unknownResponse;
	
	private final String errorResponse;
	
	private final String endCommand;

	/**
	 * @param socket
	 * @param protocol
	 * @param commandExecutor
	 */
	public SocketHandlerThread(Socket socket, Protocol protocol, CommandExecutor commandExecutor)
	{
		this.socket = socket;
		this.protocol = protocol;
		this.commandExecutor = commandExecutor;
		
		this.okResponse = protocol.getOkResponse();
		this.endResponse = protocol.getEndResponse();
		this.waitingResponse = protocol.getWaitingResponse();
		this.endCommand = protocol.getEndCommand();
		this.unknownResponse = protocol.getUnknownResponse();
		this.errorResponse = protocol.getErrorResponse();
	}

	@Override
	public void run()
	{
		try
		(
			PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
			BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		)
		{
			String inputLine, outputLine;
			
			out.println(waitingResponse);
			
			while((inputLine = in.readLine()) != null)
			{
				outputLine = execute(inputLine);
				
                out.println(outputLine);
                
                if (outputLine.equals(endResponse))
                {
                    break;
                }
			}
		}
		catch (Exception e)
		{
			LOGGER.error("Error running socket handler thread.", e);
		}
		finally
		{
			if(socket != null)
			{
				try
				{
					socket.close();
					socket = null;
				}
				catch (IOException e)
				{
					LOGGER.error("Error closing socket.", e);
				}
			}
		}
	}
	
	/**
	 * Executes a command.
	 * 
	 * @param commandLine
	 */
	public String execute(String commandLine)
	{
		if(StringUtils.isBlank(commandLine))
		{
			return waitingResponse;
		}
		
		try
		{
			if(endCommand.equalsIgnoreCase(commandLine))
			{
				return endResponse;
			}
			
			Command command = protocol.translate(commandLine);
			
			commandExecutor.execute(command);
			
			LOGGER.info("Command \"" + commandLine + "\" was executed with success.");
			
			return okResponse;
		}
		catch(UnknownProtocolCommandException e)
		{
			LOGGER.error("Command \"" + commandLine + "\" is unknown.", e);
			
			return unknownResponse;
		}
		catch(Exception e)
		{
			LOGGER.error("Error executing \"" + commandLine + "\".", e);
			
			return errorResponse;
		}
	}
}
