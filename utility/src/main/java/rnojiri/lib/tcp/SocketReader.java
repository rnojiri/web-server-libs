package rnojiri.lib.tcp;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Listen for protocol commands.
 * 
 * @author rnojiri
 */
public class SocketReader
{
	private static final Logger LOGGER = LoggerFactory.getLogger(SocketReader.class);
	
	private ExecutorService executorService;
	
	private final int port;
	
	private AtomicBoolean listen;
	
	private CommandExecutor commandExecutor;
	
	private Protocol protocol;
	
	/**
	 * @param port
	 * @param executorService
	 * @param protocol
	 * @param commandExecutor
	 */
	public SocketReader(int port, ExecutorService executorService, Protocol protocol, CommandExecutor commandExecutor)
	{
		this.port = port;
		this.executorService = executorService;
		this.protocol = protocol;
		this.commandExecutor = commandExecutor;
		this.listen = new AtomicBoolean(true);
	}
	
	/**
	 * Starts to listen for commands.
	 */
	public void listen()
	{
		ServerSocket serverSocket = null;
		
		try
		{
			LOGGER.info("Starting server...");
			
			serverSocket = new ServerSocket(port);
			
			LOGGER.info("Server listening in port " + port);
			
			while(listen.get())
			{
				try
				{
					Socket clientSocket = serverSocket.accept();
					
					executorService.execute(new SocketHandlerThread(clientSocket, protocol, commandExecutor));
				}
				catch(IOException e)
				{
					LOGGER.error("Error stabilishing connection with the client.", e);
				}
			}
			
			LOGGER.info("Server stopped...");
		}
		catch(IOException e)
		{
			LOGGER.error("Error trying to listen the port " + port);
			
			throw new RuntimeException(e);
		}
		finally
		{
			if(serverSocket != null)
			{
				try
				{
					serverSocket.close();
				}
				catch(IOException e)
				{
					LOGGER.error("Error closing server socket.", e);
				}
			}
		}
	}
	
	/**
	 * Shuts down the listener.
	 */
	public void shutdown()
	{
		listen.set(false);
		
		LOGGER.info("Stoping server...");
	}
}
