package rnojiri.lib.validation.form;

import rnojiri.lib.validation.PostValidation;
import rnojiri.lib.validation.Validable;
import rnojiri.lib.validation.annotation.RequiredParam;

/**
 * Required fields test form.
 * 
 * @author rnojiri
 */
public class RequiredFieldsForm implements Validable
{
	private String stringVar;
	
	private Integer integerVar;
	
	private boolean booleanVar;

	public RequiredFieldsForm()
	{
		;
	}

	/**
	 * @return the stringVar
	 */
	@RequiredParam
	public String getStringVar()
	{
		return stringVar;
	}

	/**
	 * @param stringVar the stringVar to set
	 */
	public void setStringVar(String stringVar)
	{
		this.stringVar = stringVar;
	}

	/**
	 * @return the integerVar
	 */
	@RequiredParam
	public Integer getIntegerVar()
	{
		return integerVar;
	}

	/**
	 * @param integerVar the integerVar to set
	 */
	public void setIntegerVar(Integer integerVar)
	{
		this.integerVar = integerVar;
	}

	/**
	 * @return the booleanVar
	 */
	@RequiredParam
	public boolean isBooleanVar()
	{
		return booleanVar;
	}

	/**
	 * @param booleanVar the booleanVar to set
	 */
	public void setBooleanVar(boolean booleanVar)
	{
		this.booleanVar = booleanVar;
	}

	@Override
	public PostValidation<? extends Validable>[] getPostValidations()
	{
		// TODO Auto-generated method stub
		return null;
	}
}
