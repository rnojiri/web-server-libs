package rnojiri.lib.validation.form;

import rnojiri.lib.validation.PostValidation;
import rnojiri.lib.validation.Validable;
import rnojiri.lib.validation.annotation.RequiredParamIf;

/**
 * A form to test RequiredParamIf.
 * 
 * @author rnojiri
 */
public class RequiredParamIfForm implements Validable
{
	private String requiredValue;
	
	private String type;
	
	private Integer requiredIntegerValue;
	
	private Integer intType;
	
	public RequiredParamIfForm()
	{
		;
	}

	/**
	 * @return the requiredValue
	 */
	@RequiredParamIf(parameter = "type", value = "required")
	public String getRequiredValue()
	{
		return requiredValue;
	}

	/**
	 * @param requiredValue the requiredValue to set
	 */
	public void setRequiredValue(String requiredValue)
	{
		this.requiredValue = requiredValue;
	}

	/**
	 * @return the type
	 */
	public String getType()
	{
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type)
	{
		this.type = type;
	}

	/**
	 * @return the requiredIntegerValue
	 */
	@RequiredParamIf(parameter = "intType", value = "5")
	public Integer getRequiredIntegerValue()
	{
		return requiredIntegerValue;
	}

	/**
	 * @param requiredIntegerValue the requiredIntegerValue to set
	 */
	public void setRequiredIntegerValue(Integer requiredIntegerValue)
	{
		this.requiredIntegerValue = requiredIntegerValue;
	}

	/**
	 * @return the intType
	 */
	public Integer getIntType()
	{
		return intType;
	}

	/**
	 * @param intType the intType to set
	 */
	public void setIntType(Integer intType)
	{
		this.intType = intType;
	}

	@Override
	public PostValidation<? extends Validable>[] getPostValidations()
	{
		// TODO Auto-generated method stub
		return null;
	}
}
