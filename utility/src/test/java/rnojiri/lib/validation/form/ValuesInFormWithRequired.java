package rnojiri.lib.validation.form;

import rnojiri.lib.validation.PostValidation;
import rnojiri.lib.validation.Validable;
import rnojiri.lib.validation.annotation.RequiredParam;
import rnojiri.lib.validation.annotation.ValueIn;

/**
 * Values in form.
 * 
 * @author rnojiri
 */
public class ValuesInFormWithRequired implements Validable
{
	private String option;
	
	public ValuesInFormWithRequired()
	{
		;
	}

	/**
	 * @return the option
	 */
	@RequiredParam
	@ValueIn({"A", "B", "C"})
	public String getOption()
	{
		return option;
	}

	/**
	 * @param option the option to set
	 */
	public void setOption(String option)
	{
		this.option = option;
	}

	@Override
	public PostValidation<? extends Validable>[] getPostValidations()
	{
		// TODO Auto-generated method stub
		return null;
	}
}
