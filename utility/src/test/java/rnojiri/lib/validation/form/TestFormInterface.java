package rnojiri.lib.validation.form;

import rnojiri.lib.validation.Validable;
import rnojiri.lib.validation.annotation.RegexpValidation;
import rnojiri.lib.validation.annotation.RequiredParam;

/**
 * A interface to test an 
 * 
 * @author rnojiri
 */
public interface TestFormInterface extends Validable
{
	@RequiredParam
	String getRequired();
	
	@RequiredParam
	@RegexpValidation("[0-9]+")
	String getNumber();
}
