package rnojiri.lib.validation.form;

import java.util.List;

import rnojiri.lib.validation.PostValidation;
import rnojiri.lib.validation.Validable;
import rnojiri.lib.validation.annotation.MinimumSize;

/**
 * Minimum size form.
 * 
 * @author rnojiri
 */
public class MinimumSizeListForm implements Validable
{
	private List<String> words;
	
	public MinimumSizeListForm()
	{
		;
	}

	/**
	 * @return the words
	 */
	@MinimumSize(2)
	public List<String> getWords()
	{
		return words;
	}

	/**
	 * @param words the words to set
	 */
	public void setWords(List<String> words)
	{
		this.words = words;
	}

	@Override
	public PostValidation<? extends Validable>[] getPostValidations()
	{
		// TODO Auto-generated method stub
		return null;
	}
}
