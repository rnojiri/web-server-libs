package rnojiri.lib.util;

import java.lang.reflect.InvocationTargetException;

import org.junit.Assert;
import org.junit.Test;

import rnojiri.lib.collection.PreBuiltArrayList;
import rnojiri.lib.collection.PreBuiltHashSet;

/**
 * Tests the ObjectCopyUtilTest class.
 * 
 * @author rnojiri
 */
public class ObjectCopyUtilTest
{
	@Test
	public void testSimpleCopy() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException
	{
		Simple a = new Simple();
		
		a.setSomething1(5);
		a.setSomething2("5");
		a.setSomething3(new PreBuiltHashSet<Integer>(5));
		a.setNotOpen(5.f);
		
		Simple b = new Simple();
		
		Assert.assertEquals(0, b.getSomething1());
		Assert.assertNull(b.getSomething2());
		Assert.assertNull(b.getSomething3());
		Assert.assertEquals(new Float(0), new Float(b.getNotOpen()));
		
		ObjectCopyUtil.copyNotEqualValues(a, b);
		
		Assert.assertEquals(5, b.getSomething1());
		Assert.assertEquals("5", b.getSomething2());
		Assert.assertEquals(new PreBuiltHashSet<Integer>(5), b.getSomething3());
		Assert.assertEquals(new Float(0), new Float(b.getNotOpen()));
	}
	
	@Test
	public void testCopyDifferentProperties() throws Exception
	{
		Simple a = new Simple();
		
		a.setSomething1(3);
		a.setSomething2("1");
		a.setSomething3(new PreBuiltArrayList<Integer>(4));
		a.setNotOpen(2.f);
		a.setExceptionProp(Boolean.TRUE);
		
		Simple b = new Simple();
		
		b.setSomething1(3);
		b.setSomething2("2");
		b.setSomething3(new PreBuiltHashSet<Integer>(4));
		b.setNotOpen(3.f);
		b.setExceptionProp(Boolean.FALSE);
		
		try
		{
			ObjectCopyUtil.copyNotEqualValues(a, b);
		}
		catch(Exception e)
		{
			Assert.assertNull(e);
		}
		
		Assert.assertEquals(3, b.getSomething1());
		Assert.assertEquals("1", b.getSomething2());
		Assert.assertEquals(new PreBuiltArrayList<Integer>(4), b.getSomething3());
		Assert.assertEquals(new Float(3), new Float(b.getNotOpen()));
		Assert.assertEquals(Boolean.FALSE, b.getExceptionProp());
	}
	
	@Test
	public void testCopySimpleToInherited() throws Exception
	{
		Simple a = new Simple();
		
		a.setSomething1(1);
		a.setSomething2("2");
		a.setSomething3(new PreBuiltArrayList<Integer>(3));
		a.setNotOpen(4.f);
		a.setExceptionProp(Boolean.TRUE);
		
		Inherited b = new Inherited();
		
		ObjectCopyUtil.copyNotEqualValues(a, b);
		
		Assert.assertEquals(1, b.getSomething1());
		Assert.assertEquals("2", b.getSomething2());
		Assert.assertEquals(new PreBuiltArrayList<Integer>(3), b.getSomething3());
		Assert.assertEquals(new Float(0), new Float(b.getNotOpen()));
		Assert.assertNull(b.getExceptionProp());
		Assert.assertNull(b.getInherited1());
		Assert.assertNull(b.getInherited2());
	}
	
	@Test
	public void testCopyInheritedToSimple() throws Exception
	{
		Inherited a = new Inherited();
		
		a.setSomething1(5);
		a.setSomething2("6");
		a.setSomething3(new PreBuiltArrayList<Integer>(7));
		a.setNotOpen(8.f);
		a.setExceptionProp(Boolean.TRUE);
		a.setInherited1(9L);
		a.setInherited2((byte)10);
		
		Simple b = new Simple();
		
		ObjectCopyUtil.copyNotEqualValues(a, b);
		
		Assert.assertEquals(5, b.getSomething1());
		Assert.assertEquals("6", b.getSomething2());
		Assert.assertEquals(new PreBuiltArrayList<Integer>(7), b.getSomething3());
		Assert.assertEquals(new Float(0), new Float(b.getNotOpen()));
		Assert.assertNull(b.getExceptionProp());
	}
	
	@Test
	public void testCopyInheritedToInherited() throws Exception
	{
		Inherited a = new Inherited();
		
		a.setSomething1(11);
		a.setSomething2("12");
		a.setSomething3(new PreBuiltArrayList<Integer>(13));
		a.setNotOpen(14.f);
		a.setExceptionProp(Boolean.TRUE);
		a.setInherited1(15L);
		a.setInherited2((byte)16);
		
		Inherited b = new Inherited();
		
		ObjectCopyUtil.copyNotEqualValues(a, b);
		
		Assert.assertEquals(11, b.getSomething1());
		Assert.assertEquals("12", b.getSomething2());
		Assert.assertEquals(new PreBuiltArrayList<Integer>(13), b.getSomething3());
		Assert.assertEquals(new Float(0), new Float(b.getNotOpen()));
		Assert.assertNull(b.getExceptionProp());
		Assert.assertEquals(new Long(15), b.getInherited1());
		Assert.assertEquals(new Byte((byte)16), b.getInherited2());
	}
	
	private class Simple
	{
		private int something1;
		
		private String something2;
		
		private Object something3;
		
		private Boolean exceptionProp;
		
		private float notOpen;
		
		public Simple()
		{
			;
		}

		/**
		 * @return the something1
		 */
		public int getSomething1()
		{
			return something1;
		}

		/**
		 * @param something1 the something1 to set
		 */
		public void setSomething1(int something1)
		{
			this.something1 = something1;
		}

		/**
		 * @return the something2
		 */
		public String getSomething2()
		{
			return something2;
		}

		/**
		 * @param something2 the something2 to set
		 */
		public void setSomething2(String something2)
		{
			this.something2 = something2;
		}

		/**
		 * @return the something3
		 */
		public Object getSomething3()
		{
			return something3;
		}

		/**
		 * @param something3 the something3 to set
		 */
		public void setSomething3(Object something3)
		{
			this.something3 = something3;
		}

		/**
		 * @return the exceptionProp
		 */
		protected Boolean getExceptionProp()
		{
			return exceptionProp;
		}

		/**
		 * @param exceptionProp the exceptionProp to set
		 * @throws Exception 
		 */
		public void setExceptionProp(Boolean exceptionProp) throws Exception
		{
			if(this.exceptionProp == null)
			{
				this.exceptionProp = exceptionProp;
			}
			else
			{
				throw new Exception();
			}
		}

		/**
		 * @return the notOpen
		 */
		protected float getNotOpen()
		{
			return notOpen;
		}

		/**
		 * @param notOpen the notOpen to set
		 */
		public void setNotOpen(float notOpen)
		{
			this.notOpen = notOpen;
		}
	}
	
	private class Inherited extends Simple
	{
		private Long inherited1;
		
		private Byte inherited2;
		
		public Inherited()
		{
			super();
		}

		/**
		 * @return the inherited1
		 */
		public Long getInherited1()
		{
			return inherited1;
		}

		/**
		 * @param inherited1 the inherited1 to set
		 */
		public void setInherited1(Long inherited1)
		{
			this.inherited1 = inherited1;
		}

		/**
		 * @return the inherited2
		 */
		public Byte getInherited2()
		{
			return inherited2;
		}

		/**
		 * @param inherited2 the inherited2 to set
		 */
		public void setInherited2(Byte inherited2)
		{
			this.inherited2 = inherited2;
		}
	}
}
