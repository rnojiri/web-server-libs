package rnojiri.lib.search.solr.segmentation;

import java.util.List;

import rnojiri.lib.search.Document;

/**
 * Credit Card document.
 * 
 * @author rnojiri
 */
public class CreditCardDocument implements Document
{
	private static final long serialVersionUID = 6021146776793083495L;

	private String id;
	
	private List<String> typeCreditCardBenefit;
	
	private String typeMainHighLight;
	
	private int numCreditCardBenefits;
	
	private int numTypeCreditCardBenefits;
	
	private float holderFirstYearPriceAnnualFee;
	
	private float weightCreditCard;
	
	public CreditCardDocument()
	{
		;
	}

	@Override
	public String getId()
	{
		return id;
	}

	/**
	 * @return the typeCreditCardBenefit
	 */
	public List<String> getTypeCreditCardBenefit()
	{
		return typeCreditCardBenefit;
	}

	/**
	 * @param typeCreditCardBenefit the typeCreditCardBenefit to set
	 */
	public void setTypeCreditCardBenefit(List<String> typeCreditCardBenefit)
	{
		this.typeCreditCardBenefit = typeCreditCardBenefit;
	}

	/**
	 * @return the typeMainHighLight
	 */
	public String getTypeMainHighLight()
	{
		return typeMainHighLight;
	}

	/**
	 * @param typeMainHighLight the typeMainHighLight to set
	 */
	public void setTypeMainHighLight(String typeMainHighLight)
	{
		this.typeMainHighLight = typeMainHighLight;
	}

	/**
	 * @return the numCreditCardBenefits
	 */
	public int getNumCreditCardBenefits()
	{
		return numCreditCardBenefits;
	}

	/**
	 * @param numCreditCardBenefits the numCreditCardBenefits to set
	 */
	public void setNumCreditCardBenefits(int numCreditCardBenefits)
	{
		this.numCreditCardBenefits = numCreditCardBenefits;
	}

	/**
	 * @return the numTypeCreditCardBenefits
	 */
	public int getNumTypeCreditCardBenefits()
	{
		return numTypeCreditCardBenefits;
	}

	/**
	 * @param numTypeCreditCardBenefits the numTypeCreditCardBenefits to set
	 */
	public void setNumTypeCreditCardBenefits(int numTypeCreditCardBenefits)
	{
		this.numTypeCreditCardBenefits = numTypeCreditCardBenefits;
	}

	/**
	 * @return the holderFirstYearPriceAnnualFee
	 */
	public float getHolderFirstYearPriceAnnualFee()
	{
		return holderFirstYearPriceAnnualFee;
	}

	/**
	 * @param holderFirstYearPriceAnnualFee the holderFirstYearPriceAnnualFee to set
	 */
	public void setHolderFirstYearPriceAnnualFee(float holderFirstYearPriceAnnualFee)
	{
		this.holderFirstYearPriceAnnualFee = holderFirstYearPriceAnnualFee;
	}

	/**
	 * @return the weightCreditCard
	 */
	public float getWeightCreditCard()
	{
		return weightCreditCard;
	}

	/**
	 * @param weightCreditCard the weightCreditCard to set
	 */
	public void setWeightCreditCard(float weightCreditCard)
	{
		this.weightCreditCard = weightCreditCard;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id)
	{
		this.id = id;
	}
}
