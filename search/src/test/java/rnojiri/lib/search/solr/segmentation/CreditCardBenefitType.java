package rnojiri.lib.search.solr.segmentation;


/**
 * All credit card benefit types.
 * 
 * @author rnojiri
 */
public enum CreditCardBenefitType
{
	TICKETS("Descontos em Ingressos"),
	
	SHOPPINGCART("Descontos em Supermercados e Compras"),
		
	MOBILE("Descontos em Celular"),
	
	CAR("Descontos em Carros"),
		
	AIRPLANE("Passagens Aéreas"),
		
	SOCCER("Esportes e Clubes");
	
	/**
	 * Returns the benefit type by string.
	 * 
	 * @param benefitType
	 * @return CreditCardBenefitType
	 */
	public static CreditCardBenefitType getBenefitType(final String benefitType)
	{
		String value = benefitType.trim().toUpperCase();
		
		if(CAR.name().equals(value)) return CAR;
		
		if(AIRPLANE.name().equals(value)) return AIRPLANE;
		
		if(SOCCER.name().equals(value)) return SOCCER;

		if(SHOPPINGCART.name().equals(value)) return SHOPPINGCART;
		
		if(TICKETS.name().equals(value)) return TICKETS;
		
		if(MOBILE.name().equals(value)) return MOBILE;
				
		return null;
	}
	
	/**
	 * Returns the benefit type by portuguese string.
	 * 
	 * @param benefitTypePortuguese
	 * @return CreditCardBenefitType
	 */
	public static CreditCardBenefitType getBenefitTypeByPortuguese(final String benefitTypePortuguese)
	{				
		if(CAR.getPortuguese().equals(benefitTypePortuguese)) return CAR;

		if(AIRPLANE.getPortuguese().equals(benefitTypePortuguese)) return AIRPLANE;

		if(SOCCER.getPortuguese().equals(benefitTypePortuguese)) return SOCCER;

		if(SHOPPINGCART.getPortuguese().equals(benefitTypePortuguese)) return SHOPPINGCART;
		
		if(TICKETS.name().equals(benefitTypePortuguese)) return TICKETS;
		
		if(MOBILE.name().equals(benefitTypePortuguese)) return MOBILE;
		
		return null;
	}
	
	private final String portuguese;
	
	/**
	 * @param portuguese
	 */
	private CreditCardBenefitType(final String portuguese)
	{
		this.portuguese = portuguese;
	}
	
	/**
	 * In portuguese.
	 * 
	 * @return String
	 */
	public String getPortuguese()
	{
		return portuguese;
	}
}
