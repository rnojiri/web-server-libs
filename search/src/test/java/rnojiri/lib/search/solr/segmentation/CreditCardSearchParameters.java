package rnojiri.lib.search.solr.segmentation;

import java.util.List;

import rnojiri.lib.search.solr.service.AbstractSearchParameters;

/**
 * All credit card search parameters.
 * 
 * @author rnojiri
 */
public class CreditCardSearchParameters extends AbstractSearchParameters
{
	private List<String> relevancyBenefitType;
	
	public CreditCardSearchParameters()
	{
		;
	}

	/**
	 * @return the relevancyBenefitType
	 */
	public List<String> getRelevancyBenefitType()
	{
		return relevancyBenefitType;
	}

	/**
	 * @param relevancyBenefitType the relevancyBenefitType to set
	 */
	public void setRelevancyBenefitType(List<String> relevancyBenefitType)
	{
		this.relevancyBenefitType = relevancyBenefitType;
	}
}
