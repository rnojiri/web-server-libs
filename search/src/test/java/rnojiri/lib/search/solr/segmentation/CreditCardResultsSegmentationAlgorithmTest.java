package rnojiri.lib.search.solr.segmentation;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import rnojiri.lib.collection.PreBuiltArrayList;


/**
 * Tests the Credit Card Searcher service.
 * 
 * @author rnojiri
 */
public class CreditCardResultsSegmentationAlgorithmTest
{
	/**
	 * Creates a new dummy credit card with selected benefits.
	 * 
	 * @param benefitTypes
	 * @return CreditCardDocument
	 */
	private CreditCardDocument createCreditCard(int id, float annualFee, String benefitMainHighLight, float weightCreditCard, String... benefitTypes)
	{
		CreditCardDocument creditCardDocument = new CreditCardDocument();

		List<String> typeCreditCardBenefit = new ArrayList<String>();
		Set<String> typeCreditCardBenefitSet = new HashSet<>();

		for (String benefitType : benefitTypes)
		{
			typeCreditCardBenefit.add(benefitType);
			typeCreditCardBenefitSet.add(benefitType);
		}

		creditCardDocument.setId(String.valueOf(id));
		creditCardDocument.setTypeCreditCardBenefit(typeCreditCardBenefit);
		creditCardDocument.setTypeMainHighLight(benefitMainHighLight);
		creditCardDocument.setWeightCreditCard(weightCreditCard);
		creditCardDocument.setNumTypeCreditCardBenefits(typeCreditCardBenefitSet.size());
		creditCardDocument.setNumCreditCardBenefits(benefitTypes.length);
		creditCardDocument.setHolderFirstYearPriceAnnualFee(annualFee);

		return creditCardDocument;
	}
	
	/**
	 * Creates a list of dummy credit cards.
	 * 
	 * @return List<CreditCardDocument>
	 */
	private List<CreditCardDocument> createDummyResults()
	{
		List<CreditCardDocument> creditCards = new ArrayList<CreditCardDocument>();
		
		//Full benefits (airplane and car included)
		creditCards.add(createCreditCard(1, 110.f,
										 CreditCardBenefitType.AIRPLANE.name(), 
										 10.0f,
										 CreditCardBenefitType.CAR.name(),
										 CreditCardBenefitType.TICKETS.name(),
										 CreditCardBenefitType.TICKETS.name(),										 
										 CreditCardBenefitType.SHOPPINGCART.name(),
										 CreditCardBenefitType.SOCCER.name(),
										 CreditCardBenefitType.SHOPPINGCART.name()
										 ));
		
		//One benefit in excess (only airplane in excess, no car)
		creditCards.add(createCreditCard(2, 100.f,
										 CreditCardBenefitType.AIRPLANE.name(), 
										 50.0f,
										 CreditCardBenefitType.AIRPLANE.name(),
										 CreditCardBenefitType.AIRPLANE.name(),
										 CreditCardBenefitType.AIRPLANE.name(),
										 CreditCardBenefitType.AIRPLANE.name(),
										 CreditCardBenefitType.AIRPLANE.name(),
										 CreditCardBenefitType.AIRPLANE.name()
										 ));
		
		//Mixed few benefits (one airplane and one car)
		creditCards.add(createCreditCard(3, 100.f,
										 CreditCardBenefitType.AIRPLANE.name(),
										 10.0f,
										 CreditCardBenefitType.CAR.name()));
		
		//Mixed few benefits (doubled the above, lesser annual fee)
		creditCards.add(createCreditCard(4, 90.f,
										 CreditCardBenefitType.AIRPLANE.name(),
										 10.0f,
										 CreditCardBenefitType.CAR.name()));
		
		//No airplane benefit, only car (more annual fee)
		creditCards.add(createCreditCard(5, 110.f,
										 CreditCardBenefitType.CAR.name(),
										 10.0f,
										 CreditCardBenefitType.CAR.name()));
		
		//Neither airplane or car, but lesser benefits
		creditCards.add(createCreditCard(6, 100.f,
										 CreditCardBenefitType.SOCCER.name(),
										 10.0f,
										 CreditCardBenefitType.SHOPPINGCART.name()));

		//Neither airplane or car, but much more benefits
		creditCards.add(createCreditCard(7, 100.f,
										 CreditCardBenefitType.SOCCER.name(),
										 10.0f,
										 CreditCardBenefitType.SOCCER.name(),
										 CreditCardBenefitType.SHOPPINGCART.name(),
										 CreditCardBenefitType.SHOPPINGCART.name(),
										 CreditCardBenefitType.SHOPPINGCART.name(),
										 CreditCardBenefitType.TICKETS.name(),
										 CreditCardBenefitType.TICKETS.name(),										 
										 CreditCardBenefitType.SOCCER.name(),
										 CreditCardBenefitType.SOCCER.name(),										 
										 CreditCardBenefitType.SHOPPINGCART.name()
										 ));
		
		//Neither airplane or car, the only one with luggage
		creditCards.add(createCreditCard(8, 80.f,
										 CreditCardBenefitType.SOCCER.name(),
										 10.0f,
										 CreditCardBenefitType.SOCCER.name()));
		
		//Neither airplane or car, the only one random benefit
		creditCards.add(createCreditCard(9, 70.f,
										 CreditCardBenefitType.MOBILE.name(),
										 12.0f,
										 CreditCardBenefitType.MOBILE.name()));
		
		return creditCards;
	}
	
	/**
	 * Checks the id from credit card.
	 * 
	 * @param id
	 * @param index
	 * @param creditCardDocument
	 */
	private void assertId(int id, int index, List<CreditCardDocument> orderedResults)
	{		
		assertEquals(String.valueOf(id), orderedResults.get(index).getId());	
	}
	
	
	@Test
	public void testTwoSelectedBenefits()
	{
		CreditCardResultSegmentationAlgorithm resultSegmentationAlgorithm = new CreditCardResultSegmentationAlgorithm();
		
		CreditCardSearchParameters parameters = new CreditCardSearchParameters();
		parameters.setRelevancyBenefitType(new PreBuiltArrayList<>(new String[] {CreditCardBenefitType.MOBILE.name(),
														  						  CreditCardBenefitType.SOCCER.name()}));
		
		List<CreditCardDocument> orderedResults = resultSegmentationAlgorithm.segmentResults(createDummyResults(), parameters);
		
		int index = 0;
		
		assertId(9, index++, orderedResults);
		assertId(7, index++, orderedResults);
		assertId(8, index++, orderedResults);
		assertId(6, index++, orderedResults);
		assertId(1, index++, orderedResults);
		assertId(4, index++, orderedResults);
		assertId(2, index++, orderedResults);
		assertId(3, index++, orderedResults);
		assertId(5, index++, orderedResults);
	}
	
	
	/***
	 * Test only one benefit
	 */
	@Test
	public void testOnlyOneBenefit()
	{
		CreditCardResultSegmentationAlgorithm resultSegmentationAlgorithm = new CreditCardResultSegmentationAlgorithm();
		
		CreditCardSearchParameters parameters = new CreditCardSearchParameters();
		parameters.setRelevancyBenefitType(new PreBuiltArrayList<>(new String[] {CreditCardBenefitType.CAR.name()}));
		
		List<CreditCardDocument> orderedResults = resultSegmentationAlgorithm.segmentResults(createDummyResults(), parameters);
		
		int index = 0;
		
		assertId(5, index++, orderedResults);
		assertId(1, index++, orderedResults);
		assertId(4, index++, orderedResults);
		assertId(3, index++, orderedResults);
		assertId(7, index++, orderedResults);
		assertId(9, index++, orderedResults);
		assertId(8, index++, orderedResults);
		assertId(2, index++, orderedResults);
		assertId(6, index++, orderedResults);
	}
	
}
