package rnojiri.lib.search.segmentation;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import rnojiri.lib.search.solr.service.AbstractSearchParameters;

/**
 * An abstract segmentation algorithm for search results.
 * 
 * @author rnojiri
 *
 * @param <T>
 */
public abstract class AbstractResultSegmentationAlgorithm<T, P extends AbstractSearchParameters>
{
	private final SegmentationAlgorithmFilter<T, P>[] segmentationAlgorithmFilters; 
	
	/**
	 * Defines all segment filters.
	 * 
	 * @param segmentAlgorithmFilters
	 */
	public AbstractResultSegmentationAlgorithm()
	{
		this.segmentationAlgorithmFilters = getSegmentationAlgorithmFilters();
	}
	
	/**
	 * Transforms the segmentation algorithm filter list to array.
	 * 
	 * @return SegmentationAlgorithmFilter<T, P>[]
	 */
	private SegmentationAlgorithmFilter<T, P>[] getSegmentationAlgorithmFilters()
	{
		List<SegmentationAlgorithmFilter<T,  P>> segmentationAlgorithmFilters = createSegmentationAlgorithmFilters();
		
		@SuppressWarnings("unchecked")
		SegmentationAlgorithmFilter<T, P>[] filters = (SegmentationAlgorithmFilter<T, P>[]) new SegmentationAlgorithmFilter<?, ?>[segmentationAlgorithmFilters.size()];
		
		Iterator<SegmentationAlgorithmFilter<T, P>> iterator = segmentationAlgorithmFilters.iterator();
		
		for(int i=0; i<filters.length; i++)
		{
			filters[i] = iterator.next();
		}
		
		return filters;
	}
	
	/**
	 * Creates the segmentation algorithm filters.
	 * 
	 * @return SegmentationAlgorithmFilter<T>[]
	 */
	protected abstract List<SegmentationAlgorithmFilter<T, P>> createSegmentationAlgorithmFilters();
	
	/**
	 * Returns a list of items using the implemented segmentation algorithm.
	 * 
	 * @param list
	 * @return List<T>
	 */
	public List<T> segmentResults(List<T> list, P parameters)
	{
		if(list == null || list.size() == 0)
		{
			return list;
		}
		
		@SuppressWarnings("unchecked")
		List<T> segments[] = (List<T>[])new ArrayList<?>[segmentationAlgorithmFilters.length];
		
		for(int i=0; i<segmentationAlgorithmFilters.length; i++)
		{
			segments[i] = new ArrayList<T>();
		}

		for(T item : list)
		{
			for(int i=0; i<segmentationAlgorithmFilters.length; i++)
			{
				if(segmentationAlgorithmFilters[i].isEligible(item, parameters))
				{
					segments[i].add(item);
					
					break;
				}
			}
		}
		
		List<T> results = new ArrayList<T>();
		
		for(int i=0; i<segmentationAlgorithmFilters.length; i++)
		{
			results.addAll(segmentationAlgorithmFilters[i].postOperation(segments[i]));
		}
		
		return results;
	}
}
