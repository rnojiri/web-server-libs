package rnojiri.lib.search.comparison;

import java.util.List;

/**
 * The item comparison results.
 * 
 * @author rnojiri
 *
 * @param <T>
 * @param <R>
 */
public class ComparisonResult<T, R>
{
	private final List<T> items;
	
	private final ComparisonGridResultReader<R> comparisonGridResult;

	/**
	 * @param items
	 * @param comparisonGridResult
	 */
	public ComparisonResult(List<T> items, ComparisonGridResultReader<R> comparisonGridResult)
	{
		this.items = items;
		this.comparisonGridResult = comparisonGridResult;
	}

	/**
	 * @return the items
	 */
	public List<T> getItems()
	{
		return items;
	}

	/**
	 * @return the comparisonGridResult
	 */
	public ComparisonGridResultReader<R> getComparisonGridResult()
	{
		return comparisonGridResult;
	}
}
