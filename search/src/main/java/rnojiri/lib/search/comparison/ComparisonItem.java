package rnojiri.lib.search.comparison;

/**
 * A comparison item from the grid.
 * 
 * Use optional param for to send any relevant information that you want to show.
 * 
 * @author rnojiri
 */
public class ComparisonItem<T>
{
	private final Float weight;
	
	private final T value;
	
	private final String optional;
	
	private boolean theBestChoice;
	
	/**
	 * @param weight
	 */
	public ComparisonItem(Float weight)
	{
		this(weight, null, null);
	}

	/**
	 * @param weight
	 * @param value
	 */
	public ComparisonItem(Float weight, T value)
	{
		this(weight, value, null);
	}
	
	/**
	 * @param weight
	 * @param value
	 */
	public ComparisonItem(Float weight, T value, String optional)
	{
		this.weight = weight;
		this.value = value;
		this.optional = optional;
	}

	/**
	 * @return the weight
	 */
	public Float getWeight()
	{
		return weight;
	}

	/**
	 * @return the value
	 */
	public T getValue()
	{
		return value;
	}

	/**
	 * @return the theBestChoice
	 */
	public boolean isTheBestChoice()
	{
		return theBestChoice;
	}

	/**
	 * @param theBestChoice the theBestChoice to set
	 */
	public void setTheBestChoice(boolean theBestChoice)
	{
		this.theBestChoice = theBestChoice;
	}

	/**
	 * @return the optional
	 */
	public String getOptional()
	{
		return optional;
	}	
}
