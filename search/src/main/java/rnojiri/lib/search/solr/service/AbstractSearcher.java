package rnojiri.lib.search.solr.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.solr.client.solrj.SolrRequest.METHOD;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.slf4j.Logger;

import rnojiri.lib.collection.PreBuiltHashSet;
import rnojiri.lib.search.filter.Filter;
import rnojiri.lib.search.filter.FilterProperty;
import rnojiri.lib.search.solr.query.SolrQueryBuilder;
import rnojiri.lib.search.solr.server.CustomHttpSolrServer;
import rnojiri.lib.support.Constants;
import rnojiri.lib.support.RangedValue;

/**
 * Abstract searcher class.
 * 
 * @author rnojiri
 */
public abstract class AbstractSearcher
{
	protected final static int MAX_DOCS_PER_PAGE = 1000;
	
	protected final static Pattern FACET_SPLIT_PATTERN = Pattern.compile("\\" + Constants.PIPE);
	
	/**
	 * Returns all facet names found.
	 * 
	 * @param field
	 * @param queryResponse
	 * @return List<Count>
	 */
	protected List<Count> getFacetValuesFromField(String field, QueryResponse queryResponse)
	{		
		FacetField facetField = queryResponse.getFacetField(field);
		
		if(facetField != null)
		{
			List<Count> countList = facetField.getValues();
			
			if(countList != null && countList.size() > 0)
			{
				return countList;
			}
		}
		
		return Collections.emptyList();
	}
	
	/**
	 * Returns the results from Solr core.
	 * 
	 * @param query
	 * @param solrServer
	 * @param clazz
	 * @param method
	 * @param log
	 * @return List<T>
	 * @throws SolrServerException
	 * @throws IOException
	 */
	protected <T> List<T> getResults(SolrQueryBuilder query, CustomHttpSolrServer solrServer, Class<T> clazz, METHOD method, Logger log)
			throws SolrServerException, IOException
	{
		return getResponse(query, solrServer, method, log).getBeans(clazz);
	}

	/**
	 * Returns the QueryResponse from Solr core.
	 * 
	 * @param query
	 * @param solrServer
	 * @param method
	 * @param log
	 * @return QueryResponse
	 * @throws SolrServerException
	 * @throws IOException
	 */
	protected QueryResponse getResponse(SolrQueryBuilder query, CustomHttpSolrServer solrServer, METHOD method, Logger log) throws SolrServerException, IOException
	{
		if (log.isDebugEnabled())
		{
			log.debug("Query \"" + query.toString() + "\" created to execute in \"" + solrServer.getName() + "\".");
		}

		QueryResponse queryResponse = solrServer.query(query.getSolrQuery(), method);

		if (queryResponse.getStatus() != 0)
		{
			throw new SolrServerException("Error executing query. Received status \"" + queryResponse.getStatus() + "\" from Solr.");
		}

		return queryResponse;
	}
	
	/**
	 * Extracts a filter from facet using a set of parameters.
	 * 
	 * @param fieldName
	 * @param selectedParamSet
	 * @param queryResponse
	 * @param filterName
	 * @param filterParamName
	 * @param reorderDisabledProperties
	 * @return Filter
	 */
	protected Filter extractFilterFromCompositeFacet(final String fieldName, 
													  final Set<String> selectedParamSet, 
													  final QueryResponse queryResponse, 
													  final String filterName, 
													  final String filterParamName,
													  final boolean reorderDisabledProperties)
	{
		List<Count> facetCounts = getFacetValuesFromField(fieldName, queryResponse);
		
		List<FilterProperty> properties = new ArrayList<FilterProperty>();
		
		List<FilterProperty> disabledProperties = (reorderDisabledProperties ? new ArrayList<FilterProperty>() : null);
		
		for(int i=0; i<facetCounts.size(); i++)
		{
			Count facetCount = facetCounts.get(i);
			
			String facet[] = FACET_SPLIT_PATTERN.split(facetCount.getName());
			
			FilterProperty filterProperty = new FilterProperty(facet[1],
																facet[0],
																facetCount.getCount(),
																(selectedParamSet != null && selectedParamSet.contains(facet[1])),
																facetCount.getCount() > 0);
			
			if(reorderDisabledProperties && !filterProperty.isEnabled())
			{
				disabledProperties.add(filterProperty);
			}
			else
			{
				properties.add(filterProperty);
			}
		}
		
		if(reorderDisabledProperties && disabledProperties.size() > 0)
		{
			for(FilterProperty disabledProperty : disabledProperties)
			{
				properties.add(disabledProperty);
			}
		}
		
		return new Filter(filterName, filterParamName, properties);
	}
	
	/**
	 * Extracts a filter from facet using a set of parameters.
	 * 
	 * @param fieldName
	 * @param selectedParam
	 * @param queryResponse
	 * @param filterName
	 * @param filterParamName
	 * @return Filter
	 */
	protected Filter extractFilterFromCompositeFacet(final String fieldName, 
													  final String selectedParam, 
													  final QueryResponse queryResponse, 
													  final String filterName, 
													  final String filterParamName)
	{
		List<Count> facetCounts = getFacetValuesFromField(fieldName, queryResponse);
		
		List<FilterProperty> properties = new ArrayList<FilterProperty>();
		
		for(int i=0; i<facetCounts.size(); i++)
		{
			Count facetCount = facetCounts.get(i);
			
			String facet[] = FACET_SPLIT_PATTERN.split(facetCount.getName());
			
			properties.add(new FilterProperty(facet[1],
											  facet[0],
											  facetCount.getCount(),
											  (selectedParam != null && selectedParam.equals(facet[1])),
											  facetCount.getCount() > 0));
		}
		
		return new Filter(filterName, filterParamName, properties);
	}
	
	/**
	 * Extracts a filter from facet.
	 * 
	 * @param fieldName
	 * @param selectedParam
	 * @param queryResponse
	 * @param filterName
	 * @param filterParamName
	 * @return Filter
	 */
	protected Filter extractFilterFromFacet(final String fieldName, 
										   	final String selectedParam, 
										   	final QueryResponse queryResponse, 
										   	final String filterName, 
										   	final String filterParamName)
	{
		return extractFilterFromFacet(fieldName, 
									  PreBuiltHashSet.build(selectedParam), 
									  queryResponse, 
									  filterName, 
									  filterParamName,
									  null,
									  0);
	}
	
	/**
	 * Extracts a filter from facet.
	 * 
	 * @param fieldName
	 * @param selectedParam
	 * @param queryResponse
	 * @param filterName
	 * @param filterParamName
	 * @return Filter
	 */
	protected Filter extractFilterFromFacet(final String fieldName, 
										   	final String selectedParam, 
										   	final QueryResponse queryResponse, 
										   	final String filterName, 
										   	final String filterParamName,
										   	final int order)
	{
		return extractFilterFromFacet(fieldName, 
									  PreBuiltHashSet.build(selectedParam), 
									  queryResponse, 
									  filterName, 
									  filterParamName,
									  null,
									  order);
	}
	
	/**
	 * Extracts a filter from facet.
	 * 
	 * @param fieldName
	 * @param selectedParam
	 * @param queryResponse
	 * @param filterName
	 * @param filterParamName
	 * @param excludeFilterByNameSet
	 * @param order
	 * @return Filter
	 */
	protected Filter extractFilterFromFacet(final String fieldName, 
										   	 final String selectedParam, 
										   	 final QueryResponse queryResponse, 
										   	 final String filterName, 
										   	 final String filterParamName,
										   	 final Set<String> excludeFilterByNameSet,
										   	 final int order)
	{
		return extractFilterFromFacet(fieldName, 
									   PreBuiltHashSet.build(selectedParam), 
									   queryResponse, 
									   filterName, 
									   filterParamName,
									   excludeFilterByNameSet,
									   order);
	}
	
	/**
	 * Extracts a filter from facet.
	 * 
	 * @param fieldName
	 * @param selectedParam
	 * @param queryResponse
	 * @param filterName
	 * @param filterParamName
	 * @return Filter
	 */
	protected Filter extractFilterFromFacet(final String fieldName, 
										   	 final String selectedParam, 
										   	 final QueryResponse queryResponse, 
										   	 final String filterName, 
										   	 final String filterParamName,
										   	 final Set<String> excludeFilterByNameSet)
	{
		return extractFilterFromFacet(fieldName, 
									   PreBuiltHashSet.build(selectedParam), 
									   queryResponse, 
									   filterName, 
									   filterParamName,
									   excludeFilterByNameSet,
									   0);
	}
	
	/**
	 * Extracts a filter from facet.
	 * 
	 * @param fieldName
	 * @param selectedParam
	 * @param queryResponse
	 * @param filterName
	 * @param filterParamName
	 * @return Filter
	 */
	protected Filter extractFilterFromFacet(final String fieldName,
											final Set<String> selectedParamSet,
											final QueryResponse queryResponse,
											final String filterName,
											final String filterParamName)
	{
		return extractFilterFromFacet(fieldName, 
										selectedParamSet, 
										queryResponse, 
										filterName, 
										filterParamName, 
										null,
										0);
	}
	
	/**
	 * Extracts a filter from facet.
	 * 
	 * @param fieldName
	 * @param selectedParam
	 * @param queryResponse
	 * @param filterName
	 * @param filterParamName
	 * @return Filter
	 */
	protected Filter extractFilterFromFacet(final String fieldName,
											final Set<String> selectedParamSet,
											final QueryResponse queryResponse,
											final String filterName,
											final String filterParamName,
											final int order)
	{
		return extractFilterFromFacet(fieldName, 
										selectedParamSet, 
										queryResponse, 
										filterName, 
										filterParamName, 
										null,
										order);
	}
	
	/**
	 * Extracts a filter from facet.
	 * 
	 * @param fieldName
	 * @param selectedParam
	 * @param queryResponse
	 * @param filterName
	 * @param filterParamName
	 * @param excludeFilterByNameSet
	 * @return Filter
	 */
	protected Filter extractFilterFromFacet(final String fieldName, 
										   	final Set<String> selectedParamSet, 
										   	final QueryResponse queryResponse, 
										   	final String filterName, 
										   	final String filterParamName,
										   	final Set<String> excludeFilterByNameSet)
	{
		return extractFilterFromFacet(fieldName,
									  selectedParamSet,
									  queryResponse,
									  filterName,
									  filterParamName,
									  excludeFilterByNameSet,
									  0);
	}
	
	/**
	 * Extracts a filter from facet.
	 * 
	 * @param fieldName
	 * @param selectedParam
	 * @param queryResponse
	 * @param filterName
	 * @param filterParamName
	 * @param excludeFilterByNameSet
	 * @param order
	 * @return Filter
	 */
	protected Filter extractFilterFromFacet(final String fieldName, 
										   	final Set<String> selectedParamSet, 
										   	final QueryResponse queryResponse, 
										   	final String filterName, 
										   	final String filterParamName,
										   	final Set<String> excludeFilterByNameSet,
										   	final int order)
	{
		List<Count> facetCounts = getFacetValuesFromField(fieldName, queryResponse);
		
		if (excludeFilterByNameSet != null)
		{
			for (int i = 0; i < facetCounts.size(); i++)
			{
				if (excludeFilterByNameSet.contains(facetCounts.get(i).getName().toUpperCase()))
				{
					facetCounts.remove(i);
					i--;
				}
			}
		}
		
		List<FilterProperty> properties = new ArrayList<FilterProperty>();
		
		for(int i=0; i<facetCounts.size(); i++)
		{
			Count facetCount = facetCounts.get(i);
			
			String facet = facetCount.getName().toUpperCase();
			
			properties.add(new FilterProperty(facet,
							  				  facet,
							  				  facetCount.getCount(),
							  				  (selectedParamSet != null && selectedParamSet.contains(facet)),
							  				  facetCount.getCount() > 0));
		}
		
		return new Filter(filterName, filterParamName, properties, order);
	}
	
	/**
	 * Extracts the filter from query facets.
	 * 
	 * @param selectedParam
	 * @param labels
	 * @param rangeMap
	 * @param queryResponse
	 * @param filterName
	 * @param filterParamName
	 * @return Filter
	 */
	protected Filter extractFilterFromQueryFacet(final String selectedParam, 
												  final String labels[], 
												  final Map<String, RangedValue<Number>> rangeMap, 
												  final QueryResponse queryResponse, 
												  final String filterName, 
												  final String filterParamName)
	{
		return extractFilterFromQueryFacet(PreBuiltHashSet.build(selectedParam),
											labels,
											rangeMap,
											queryResponse,
											filterName,
											filterParamName);
	}
	
	/**
	 * Extracts the filter from query facets.
	 * 
	 * @param selectedParamSet
	 * @param labels
	 * @param rangeMap
	 * @param queryResponse
	 * @param filterName
	 * @param filterParamName
	 * @param order
	 * @return Filter
	 */
	protected <T extends Number> Filter extractFilterFromQueryFacet(final Set<String> selectedParamSet, 
	                                                                final String labels[], 
	                                                                final Map<String, RangedValue<T>> rangeMap, 
	                                                                final QueryResponse queryResponse, 
	                                                                final String filterName, 
	                                                                final String filterParamName,
	                                                                final int order)
	{
		Map<String, Integer> facetResultMap = queryResponse.getFacetQuery();
		
		List<FilterProperty> properties = new ArrayList<FilterProperty>();
		
		Iterator<String> iterator = rangeMap.keySet().iterator();
		
		for(int i=0; i<labels.length; i++)
		{
			String rangeKey = iterator.next();
			
			long count = facetResultMap.get(rangeKey).intValue();
			
			properties.add(new FilterProperty(rangeKey, 
											  labels[i], 
											  count,
											  (selectedParamSet != null && selectedParamSet.contains(rangeKey)), 
											  count > 0));
		}
		
		return new Filter(filterName, filterParamName, properties, order);
	}
	
	/**
	 * Extracts the filter from query facets.
	 * 
	 * @param selectedParamSet
	 * @param labels
	 * @param rangeMap
	 * @param queryResponse
	 * @param filterName
	 * @param filterParamName
	 * @return Filter
	 */
	protected <T extends Number> Filter extractFilterFromQueryFacet(final Set<String> selectedParamSet, 
	                                                                final String labels[], 
	                                                                final Map<String, RangedValue<T>> rangeMap, 
	                                                                final QueryResponse queryResponse, 
	                                                                final String filterName, 
	                                                                final String filterParamName)
	{
		return extractFilterFromQueryFacet(selectedParamSet,
										   labels,
										   rangeMap,
										   queryResponse,
										   filterName,
										   filterParamName,
										   0);
	}

	/**
	 * Checks if sort asc.
	 * 
	 * @param parameters
	 * @return boolean
	 */
	protected boolean sortAsc(AbstractSearchParameters parameters)
	{
		String sortMode = parameters.getSortMode();
		
		if(AbstractSearchParameters.SORT_MODE_ASC.equals(sortMode))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
