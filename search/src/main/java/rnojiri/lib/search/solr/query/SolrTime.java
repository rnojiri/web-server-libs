package rnojiri.lib.search.solr.query;

import rnojiri.lib.support.Constants;

/**
 * Handles the time values used in solr queries.
 * 
 * @author rnojiri
 */
public class SolrTime
{

	/**
	 * Measures.
	 * 
	 * @author rnojiri
	 */
	public static enum Measure
	{
		DAY, MONTH, YEAR, NOW
	}

	private StringBuilder stringBuilder = new StringBuilder(Constants.BUFFER_SIZE_32);

	private static final char SIGN_PLUS = '+';
	private static final char SIGN_MINUS = '-';
	private static final char SIGN_ROUND = '/';

	private boolean nowWasAdded = false;

	/**
	 * Uses NOW as default.
	 */
	public SolrTime()
	{
		super();
		stringBuilder.append(Measure.NOW);
		nowWasAdded = true;
	}

	/**
	 * Sets a value and a measure.
	 * 
	 * (the value can be negative)
	 * 
	 * @param value
	 * @param measure
	 */
	public SolrTime(int value, Measure measure)
	{
		super();
		addValue(value, measure);
	}

	/**
	 * Sets the time to NOW.
	 * 
	 * @param value
	 * @param measure
	 */
	public SolrTime setNow()
	{
		return setValue(0, Measure.NOW);
	}

	/**
	 * Sets the time to the specified.
	 * 
	 * @param value
	 * @param measure
	 */
	public SolrTime setValue(int value, Measure measure)
	{
		stringBuilder.delete(0, stringBuilder.capacity());

		nowWasAdded = false;

		return addValue(value, measure);
	}

	/**
	 * Add a time value.
	 * 
	 * @param value
	 * @param measure
	 */
	public SolrTime addValue(int value, Measure measure)
	{
		if (Measure.NOW.equals(measure))
		{
			if (!nowWasAdded)
			{
				stringBuilder.append(Measure.NOW);
				nowWasAdded = true;
			}

			return this;
		}

		stringBuilder.append((value >= 0) ? SIGN_PLUS : SIGN_MINUS);
		stringBuilder.append(Math.abs(value));
		stringBuilder.append(measure.name());

		return this;
	}

	/**
	 * Returns the transformed value.
	 * 
	 * @return String
	 */
	public String getValue()
	{
		return stringBuilder.toString();
	}

	/**
	 * Rounds the time based on its measure.
	 * 
	 * @param measure
	 * @return SolrTime
	 */
	public SolrTime round(Measure measure)
	{
		if (!Measure.NOW.equals(measure))
		{
			stringBuilder.append(SIGN_ROUND);
			stringBuilder.append(measure.name());
		}

		return this;
	}
}
