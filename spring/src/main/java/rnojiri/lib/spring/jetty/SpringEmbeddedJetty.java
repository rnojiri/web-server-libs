package rnojiri.lib.spring.jetty;

import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import javax.servlet.DispatcherType;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpServlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.DispatcherServlet;

import rnojiri.jetty.AbstractEmbeddedJetty;
import rnojiri.jetty.FilterItem;
import rnojiri.lib.util.PropertiesManager;
import rnojiri.lib.util.PropertiesReader;

/**
 * A embbeded jetty with Spring.
 * 
 * @author rnojiri
 */
public abstract class SpringEmbeddedJetty extends AbstractEmbeddedJetty
{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SpringEmbeddedJetty.class);
	
	protected AnnotationConfigWebApplicationContext springContext;
    
	/**
	 * @param propertiesFile
	 * @throws IOException
	 */
    public SpringEmbeddedJetty(PropertiesReader properties) throws IOException
	{
		super(properties);
	}
    
    @Override
    protected List<FilterItem> getFilters()
    {
    	CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
		characterEncodingFilter.setBeanName("characterEncodingFilter");
		characterEncodingFilter.setEncoding("UTF-8");
		
		List<FilterItem> filters = new ArrayList<>();
		filters.add(new FilterItem(characterEncodingFilter, "/*", EnumSet.of(DispatcherType.REQUEST)));
    	
    	return filters;
    }
  
    /**
     * Returns the spring context.
     * 
     * @return AnnotationConfigWebApplicationContext
     */
    private AnnotationConfigWebApplicationContext getSpringContext()
    {
    	if(this.springContext != null)
    	{
    		return this.springContext;
    	}
    	
    	String file = (String)properties.getOrDefault("spring.application.properties.file", "application.properties");
		
    	PropertiesReader springProperties = null;
    	
    	try
    	{
    		springProperties = PropertiesManager.getInstance().loadProperties(file, true);
    	}
    	catch (IOException e) 
    	{
			throw new RuntimeException(e);
		}
		
		this.springContext = new AnnotationConfigWebApplicationContext() 
		{
			@Override
			protected void loadBeanDefinitions(DefaultListableBeanFactory beanFactory)
			{
				beanFactory.registerSingleton("cacheInstance", cacheInstance);
				
				super.loadBeanDefinitions(beanFactory);
			}
		};
		
		this.springContext.scan(springProperties.getArray("spring.config.packages"));
		
		return this.springContext; 
    }
	
	@Override
	protected HttpServlet getHttpServlet()
	{
		return new DispatcherServlet(getSpringContext());
	}

	@Override
	protected ServletContextListener getServletContextListener()
	{
        return new ContextLoaderListener(getSpringContext());
	}
	
	@Override
	protected Logger getLogger()
	{
		return LOGGER;
	}
}
