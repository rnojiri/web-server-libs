package rnojiri.lib.spring.controller;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.springframework.web.servlet.ModelAndView;

import rnojiri.lib.support.CommonCodeResults;
import rnojiri.lib.support.JsonResult;
import rnojiri.lib.validation.Validable;
import rnojiri.lib.validation.Validator;

/**
 * A parent controller with useful functions.
 * 
 * @author rnojiri
 */
public abstract class AbstractController
{
	private static final String HAS_ERROR = "hasError";
	
	private static final String MESSAGE = "message";
	
	private static final String INVALID_FIELD_MAP = "invalidFieldMap";
	
	/**
	 * Checks if the form is valid and puts the validation results to the model and view object.
	 * 
	 * @param validable
	 * @param modelAndView
	 * @return boolean
	 */
	protected boolean isValid(ModelAndView modelAndView, Validable... validable)
	{
		Map<String, String> invalidFieldMap = Validator.verify(validable);
		
		boolean isValid = invalidFieldMap.isEmpty();
		
		if(!isValid)
		{
			modelAndView.addObject(HAS_ERROR, true);
			modelAndView.addObject(MESSAGE, CommonCodeResults.VALIDATION_FAILURE);
			modelAndView.addObject(INVALID_FIELD_MAP, invalidFieldMap);
		}
		
		return isValid;
	}
	
	/**
	 * Checks if the form is valid and returns a error response JSON if not.
	 * 
	 * @param validable
	 * @param jsonResult
	 * @param response
	 * @return boolean
	 */
	protected boolean isValid(JsonResult<String, Object> jsonResult, HttpServletResponse response, Validable ...validable)
	{
		Map<String, String> invalidFieldMap = Validator.verify(validable);
		
		boolean isValid = invalidFieldMap.isEmpty();
		
		if(!isValid)
		{
			jsonResult.setAll(CommonCodeResults.VALIDATION_FAILURE, invalidFieldMap);
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
		
		return isValid;
	}
	
	/**
	 * Adds a success status
	 * 
	 * @param modelAndView
	 */
	protected void addSucess(ModelAndView modelAndView, String success)
	{
		modelAndView.addObject(HAS_ERROR, false);
		modelAndView.addObject(MESSAGE, success);
	}
	
	/**
	 * Adds a success status and data.
	 * 
	 * @param jsonResult
	 * @param data
	 */
	protected void addSucess(JsonResult<String, Object> jsonResult, Object data)
	{
		jsonResult.setAll(CommonCodeResults.SUCCESS, data);
	}
	
	/**
	 * Adds a success status.
	 * 
	 * @param jsonResult
	 */
	protected void addSucess(JsonResult<String, Object> jsonResult)
	{
		jsonResult.setAll(CommonCodeResults.SUCCESS, null);
	}
	
	/**
	 * Adds an error.
	 * 
	 * @param modelAndView
	 * @param error
	 */
	protected void addGenericError(ModelAndView modelAndView, String error)
	{
		modelAndView.addObject(HAS_ERROR, true);
		modelAndView.addObject(MESSAGE, error);
	}
	
	/**
	 * Adds an error.
	 * 
	 * @param jsonResult
	 * @param error
	 * @param response
	 */
	protected void addGenericError(JsonResult<String, Object> jsonResult, String error, HttpServletResponse response)
	{
		addGenericResult(jsonResult, error, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, response);
	}
	
	/**
	 * Adds a generic result.
	 * 
	 * @param jsonResult
	 * @param resultCode
	 * @param statusCode
	 * @param response
	 */
	protected void addGenericResult(JsonResult<String, Object> jsonResult, String resultCode, int statusCode, HttpServletResponse response)
	{
		jsonResult.setResultCode(resultCode);
		response.setStatus(statusCode);
	}
	
	/**
	 * Returns the class logger.
	 * 
	 * @return Logger
	 */
	protected abstract Logger getLogger();
}
