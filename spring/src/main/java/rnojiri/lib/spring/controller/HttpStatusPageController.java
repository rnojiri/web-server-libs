package rnojiri.lib.spring.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Has all common http status pages.
 * 
 * @author rnojiri
 */
@Controller
@RequestMapping("/http-status")
public class HttpStatusPageController
{
	public static final String HTTP_STATUS_CONTEXT = "/http-status";
	
	public static final String HTTP_STATUS_404 = HTTP_STATUS_CONTEXT + "/404";
	
	public static final String HTTP_STATUS_500 = HTTP_STATUS_CONTEXT + "/500";
	
	/**
	 * Returns the http response status.
	 */
	@RequestMapping(value="/{code}", method=RequestMethod.GET)
	public void status(@PathVariable int code, HttpServletResponse response)
	{
		response.setStatus(code);
	}
	
	/**
	 * Returns the http response page.
	 * 
	 * @return ModelAndView
	 */
	@RequestMapping(value="/p/{code}", method=RequestMethod.GET)
	public ModelAndView page(@PathVariable int code, HttpServletResponse response)
	{
		response.setStatus(code);
		
		return new ModelAndView("/http-status/" + code);
	}
}
