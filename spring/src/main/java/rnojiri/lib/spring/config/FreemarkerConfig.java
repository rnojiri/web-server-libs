package rnojiri.lib.spring.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;
import org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver;

import rnojiri.lib.log.BeanLogger;
import rnojiri.lib.util.PropertiesManager;
import rnojiri.lib.util.PropertiesReader;
import rnojiri.lib.util.UnsupportedTypeException;

/**
 * Freemarker configuration.
 * 
 * @author rnojiri
 */
@Configuration
public class FreemarkerConfig
{
	/**
	 * The freemarker configurer bean.
	 * 
	 * @return FreeMarkerConfigurer
	 */
	@Bean
	@Autowired
	public FreeMarkerConfigurer freeMarkerConfigurer()
	{
		BeanLogger.creating(FreeMarkerConfigurer.class);
		
		PropertiesReader properties = PropertiesManager.getInstance().getMainProperties();
		
		FreeMarkerConfigurer configurer = new FreeMarkerConfigurer();
		configurer.setTemplateLoaderPath(properties.getProperty("template.loader.path"));
		configurer.setDefaultEncoding(properties.getProperty("default.encoding"));
		
		return configurer;
	}

	/**
	 * The freemarker view resolver.
	 * 
	 * @return ViewResolver
	 * @throws UnsupportedTypeException 
	 */
	@Bean
	@Autowired
	public ViewResolver freeMarkerViewResolver() throws UnsupportedTypeException
	{
		BeanLogger.creating(ViewResolver.class);
		
		PropertiesReader properties = PropertiesManager.getInstance().getMainProperties();
		
		FreeMarkerViewResolver viewResolver = new FreeMarkerViewResolver();
		viewResolver.setCache(properties.getCastedProperty(Boolean.class, "enable.cache"));
		viewResolver.setPrefix(properties.getProperty("view.prefix"));
		viewResolver.setSuffix(properties.getProperty("view.suffix"));
		viewResolver.setContentType(properties.getProperty("content.type"));
		viewResolver.setExposeSpringMacroHelpers(properties.getCastedProperty(Boolean.class, "expose.spring.macro.helpers"));
		viewResolver.setExposeRequestAttributes(properties.getCastedProperty(Boolean.class, "expose.request.attributes"));
		viewResolver.setExposeSessionAttributes(properties.getCastedProperty(Boolean.class, "expose.session.attributes"));
		
		return viewResolver;
	}

}