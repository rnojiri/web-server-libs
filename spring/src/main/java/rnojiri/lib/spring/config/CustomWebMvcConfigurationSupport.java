package rnojiri.lib.spring.config;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.config.annotation.CorsRegistration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import rnojiri.lib.util.PropertiesManager;
import rnojiri.lib.util.PropertiesReader;
import rnojiri.lib.util.UnsupportedTypeException;

/**
 * The main configuration for the spring servlet.
 * 
 * @author rnojiri
 */
@Order(2)
@Configuration
public class CustomWebMvcConfigurationSupport extends WebMvcConfigurationSupport implements ApplicationContextAware, ServletContextAware
{
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomWebMvcConfigurationSupport.class);
	
	public CustomWebMvcConfigurationSupport()
	{
		LOGGER.info("Web MVC configuration instantiated...");
	}
	
	@Override
	protected void addInterceptors(InterceptorRegistry registry)
	{
		addInterceptorsFromBean("customInterceptors", registry);
	}
	
	/**
	 * Add interceptors to the registry.
	 * 
	 * @param beanName
	 * @param registry
	 */
	private void addInterceptorsFromBean(String beanName, InterceptorRegistry registry)
	{
		try
		{
			@SuppressWarnings("unchecked")
			List<HandlerInterceptorAdapter> customInterceptors = (List<HandlerInterceptorAdapter>)getApplicationContext().getBean(beanName);
			
			if(customInterceptors != null && customInterceptors.size() > 0)
			{
				for(HandlerInterceptorAdapter interceptor : customInterceptors)
				{
					LOGGER.info("Adding a new interceptor {}", interceptor.getClass().getName());
					
					registry.addInterceptor(interceptor);
				}
			}
		}
		catch(NoSuchBeanDefinitionException e)
		{
			LOGGER.info("Bean {} not found.", beanName);
		}
	}
	
	@Override
	protected void addCorsMappings(CorsRegistry registry)
	{
		PropertiesReader propertiesReader = PropertiesManager.getInstance().getMainProperties();
		
		boolean enableGlobalCors = Boolean.parseBoolean(propertiesReader.getProperty("cors.global.enable", Boolean.FALSE.toString()));
		
		if(enableGlobalCors)
		{
			LOGGER.info("Global CORS is enabled!");
			
			final boolean allowCredentials = Boolean.parseBoolean(propertiesReader.getProperty("cors.global.allow.credentials", Boolean.FALSE.toString()));
			LOGGER.info("Credentials are " + (allowCredentials ? "enabled" : "disabled") + " in the global CORS configuration.");
			
			final String mappings[] = getArray(propertiesReader, "cors.global.mappings", "/**");
			LOGGER.info("Adding mappings " + Arrays.toString(mappings) + " to the global CORS configuration.");
			
			final String headers[] = getArray(propertiesReader, "cors.global.allowed.headers", "*");
			LOGGER.info("Adding headers " + Arrays.toString(headers) + " to the global CORS configuration.");
			
			final String methods[] = getArray(propertiesReader, "cors.global.allowed.methods", "*");
			LOGGER.info("Adding methods " + Arrays.toString(methods) + " to the global CORS configuration.");
			
			final String origins[] = getArray(propertiesReader, "cors.global.allowed.origins", "*");
			LOGGER.info("Adding origins " + Arrays.toString(origins) + " to the global CORS configuration.");
			
			Long maxAge = null;
			
			try
			{
				maxAge = propertiesReader.getCastedProperty(Long.class, "cors.global.max.age");
			}
			catch (UnsupportedTypeException e) 
			{
				LOGGER.warn("Error converting 'cors.global.max.age' to long", e);
			}
			
			for(String mapping : mappings)
			{
				CorsRegistration registration = registry.addMapping(mapping);
				registration.allowCredentials(allowCredentials);
				registration.allowedHeaders(headers);
				registration.allowedMethods(methods);
				registration.allowedOrigins(origins);
				if(maxAge != null) registration.maxAge(maxAge);
			}
		}
		
		super.addCorsMappings(registry);
	}
	
	/**
	 * Returns an array from the properties file, setting some default value if none.
	 * 
	 * @param propertiesReader
	 * @param key
	 * @param defaultValue
	 * @return String[]
	 */
	private String[] getArray(PropertiesReader propertiesReader, String key, String defaultValue)
	{
		String array[] = propertiesReader.getArray(key);
		
		if(array == null || array.length == 0)
		{
			array = new String[] {defaultValue};
		}
		
		return array;
	}
}
