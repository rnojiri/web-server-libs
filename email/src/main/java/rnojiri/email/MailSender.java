package rnojiri.email;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import freemarker.template.TemplateException;
import rnojiri.lib.interfaces.AsyncCallbackInterface;
import rnojiri.lib.support.Constants;

/**
 * A mail sender using a specific provider implementation.
 * 
 * @author rnojiri
 */
public class MailSender extends AbstractMailSender
{
	private static Logger LOGGER = LoggerFactory.getLogger(MailSender.class);
	
	private Session session;

	private TemplateRenderer templateRenderer;
	
	private boolean isLocal;
	
	private String localEmailPath;
	
	private static final String LOCAL_EMAIL_EXTENSION = ".html"; 
	
	private final String hostName;
	
	private final String userName;
	
	private final String password;

	/**
	 * Configures the mail sender.
	 * 
	 * @param templateRenderer
	 * @param mailSenderParameters
	 * @param maxNumThreads
	 * @param localEnvironment
	 * @param localEmailPath
	 * @throws IOException
	 */
	public MailSender(TemplateRenderer templateRenderer, MailSenderParameters mailSenderParameters, int maxNumThreads, boolean localEnvironment, String localEmailPath) throws IOException
	{
		super(maxNumThreads);
		
		this.templateRenderer = templateRenderer;

		// Create a Session object to represent a mail session with the specified properties.
		this.session = Session.getDefaultInstance(mailSenderParameters.getConnectionProperties());
		
		this.isLocal = localEnvironment;
		
		LOGGER.info("Using local environment settings: " + isLocal);
		
		this.localEmailPath = localEmailPath;
		
		this.hostName = mailSenderParameters.getHostName();
		this.userName = mailSenderParameters.getUserName();
		this.password = mailSenderParameters.getPassword();
	}
	
	/**
	 * Returns an array of recipient addresses. 
	 * 
	 * @param addresses
	 * @return InternetAddress[]
	 * @throws MessagingException 
	 */
	private void setRecipients(RecipientType recipientType, MimeMessage message, Set<String> addresses) throws MessagingException
	{
		if(addresses == null || addresses.isEmpty())
		{
			return;
		}
		
		InternetAddress array[] = new InternetAddress[addresses.size()];
		
		Iterator<String> iterator = addresses.iterator();
		
		for(int i=0; i<array.length; i++)
		{
			array[i] = new InternetAddress(iterator.next());
		}
		
		if(RecipientType.TO.equals(recipientType))
		{
			if(LOGGER.isDebugEnabled())
			{
				LOGGER.debug("Adding e-mail addresses " + addresses.toString() + " with type TO.");
			}
			
			message.setRecipients(RecipientType.TO, array);
		}
		else if(RecipientType.CC.equals(recipientType))
		{
			if(LOGGER.isDebugEnabled())
			{
				LOGGER.debug("Adding e-mail addresses " + addresses.toString() + " with type CC.");
			}
			
			message.setRecipients(RecipientType.CC, array);
		}
		else if(RecipientType.BCC.equals(recipientType))
		{
			if(LOGGER.isDebugEnabled())
			{
				LOGGER.debug("Adding e-mail addresses " + addresses.toString() + " with type BCC.");
			}
			
			message.setRecipients(RecipientType.BCC, array);
		}
	}

	/**
	 * Configures the transport.
	 * 
	 * @param transport
	 * @throws MessagingException
	 */
	protected void configureTransport(Transport transport) throws MessagingException
	{
        transport.connect(hostName, userName, password);
	}
	
	/**
	 * Creates the MIME message.
	 * 
	 * @return MimeMessage
	 * @throws MessagingException
	 * @throws AddressException
	 * @throws IOException
	 * @throws TemplateException
	 */
	private MimeMessage createMimeMessage(final Email email) throws MessagingException, AddressException, IOException, TemplateException
	{
		MimeMessage message = new MimeMessage(session);
		message.setFrom(new InternetAddress(email.getFrom()));
		setRecipients(RecipientType.TO, message, email.getTo());
		setRecipients(RecipientType.CC, message, email.getCc());
		setRecipients(RecipientType.BCC, message, email.getBcc());
		message.setSubject(email.getSubject(), Constants.UTF_8);
		
		String text = templateRenderer.renderTemplateToString(email.getTemplate(), email.getVariableMap());
		String textContentType = email.getContentType().getValue();
		
		File attachmentFile = email.getAttachment();
		
		if(attachmentFile == null)
		{
			message.setContent(text, textContentType);
		}
		else
		{
		    MimeBodyPart messageBodyPart = new MimeBodyPart();
		    messageBodyPart.setText(textContentType);

		    Multipart multipart = new MimeMultipart();
		    multipart.addBodyPart(messageBodyPart);

		    messageBodyPart = new MimeBodyPart();
		    DataSource source = new FileDataSource(attachmentFile);
		    messageBodyPart.setDataHandler(new DataHandler(source));
		    messageBodyPart.setFileName(attachmentFile.getName());
		    
		    multipart.addBodyPart(messageBodyPart);
		    
		    message.setContent(multipart);
		}
		
		return message;
	}

	@Override
	protected boolean sendMail(final Email email, final AsyncCallbackInterface asyncCallbackInterface)
	{
		boolean wasSent = false;
		
		Transport transport = null;
		
		try
		{
			if(!isLocal)
			{
				// Create a transport.
				transport = session.getTransport();
				
				// Create a message with the specified information.
				MimeMessage msg = createMimeMessage(email);
				
				if(LOGGER.isDebugEnabled())
				{
					LOGGER.debug("Attempting to send an email through the Amazon SES SMTP interface...");
				}

				// Connects to the SMTP service
				configureTransport(transport);

				// Send the email.
				transport.sendMessage(msg, msg.getAllRecipients());
				
				wasSent = true;
			}
			else 
			{
				String filePath = new StringBuilder(Constants.BUFFER_SIZE_64).append(localEmailPath)
																			 .append('/')
																			 .append(System.currentTimeMillis())
																			 .append(LOCAL_EMAIL_EXTENSION)
																			 .toString();
				
				templateRenderer.renderTemplateToFile(email.getTemplate(), email.getVariableMap(), filePath);
				
				LOGGER.info("E-mail sending was canceled in local environment, saving a html instead.");
				
				wasSent = true;
			}
			
			LOGGER.info(new StringBuilder(Constants.BUFFER_SIZE_256).append("E-mail from \"").append(email.getFrom())
																 .append("\" was sent to \"").append(email.getTo())
																 .append("\" using template \"").append(email.getTemplate())
																 .append("\".").toString());
		}
		catch(AddressException e)
		{
			LOGGER.error("Invalid e-mail address: " + email.getTo(), e);
			
			asyncCallbackInterface.handleException(e);
			
			wasSent = true;
		}
		catch(NoSuchProviderException e)
		{
			LOGGER.error("Error creating a new transport.", e);
			
			asyncCallbackInterface.handleException(e);
		}
		catch(MessagingException e) 
		{
			LOGGER.error("Error sending e-mail.", e);
			
			asyncCallbackInterface.handleException(e);
		}
		catch (TemplateException e) 
		{
			LOGGER.error("Error rendering template.", e);
			
			asyncCallbackInterface.handleException(e);
		}
		catch (IOException e) 
		{
			LOGGER.error("Error loading template.", e);
			
			asyncCallbackInterface.handleException(e);
		}
		finally
		{
			if(transport != null)
			{
				try
				{
					transport.close();
				}
				catch(MessagingException e)
				{
					LOGGER.error("Error closing transport.", e);
				}
			}
		}
		
		if(wasSent)
		{
			asyncCallbackInterface.handleSuccess();
		}
		
		return wasSent;
	}

	@Override
	public Logger getLogger()
	{
		return LOGGER;
	}
}
