package rnojiri.email;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import rnojiri.lib.freemarker.tool.FreemarkerTool;
import rnojiri.lib.freemarker.tool.factory.ToolFactory;
import rnojiri.lib.support.Constants;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * A template renderer class using FreeMarker engine.
 * 
 * @author rnojiri
 */
public class TemplateRenderer
{
    private Configuration configuration;
    
    private static final int BUFFER_SIZE = 80000;
    
    private static Logger log = LoggerFactory.getLogger(TemplateRenderer.class);
    
    private List<ToolFactory<?>> toolFactoryList;
    
    /**
     * Creates a new renderer with the given path.
     * 
     * @param templatesPath
     * @param toolFactoryList
     * @throws IOException
     */
    public TemplateRenderer(String templatesPath, List<ToolFactory<?>> toolFactoryList) throws IOException 
	{
		configuration = new Configuration(Configuration.VERSION_2_3_21);
		configuration.setDirectoryForTemplateLoading(new File(templatesPath));
		configuration.setDefaultEncoding(Constants.UTF_8);
		
		this.toolFactoryList = toolFactoryList;
		
		log.info("Template Renderer was configured to use path: \"" + templatesPath + "\".");
	}
    
    /**
     * Merges the data model and the template to renders to String. 
     * 
     * @param templateName
     * @param dataModel
     * @return String
     * @throws IOException
     * @throws TemplateException
     */
    public String renderTemplateToString(String templateName, Map<String, Object> dataModel) throws IOException, TemplateException
    {
    	if(dataModel == null) dataModel = new HashMap<>();    		
    	
    	addTools(dataModel);
    	
    	Template template = configuration.getTemplate(templateName);
    	
    	StringWriter stringWriter = new StringWriter(BUFFER_SIZE);
    	
    	template.process(dataModel, stringWriter);
    	
    	return stringWriter.toString();
    }
    
    /**
     * Merges the data model and the template to renders to file. 
     * 
     * @param templateName
     * @param dataModel
     * @param file
     * @throws IOException
     * @throws TemplateException
     */
    public void renderTemplateToFile(String templateName, Map<String, Object> dataModel, String filePath) throws IOException, TemplateException
    {
    	if(dataModel == null) dataModel = new HashMap<>();
    	
    	addTools(dataModel);
    	
    	Template template = configuration.getTemplate(templateName);
    	
    	FileWriter fileWriter = new FileWriter(new File(filePath));
    	
    	template.process(dataModel, fileWriter);
    	
    	fileWriter.flush();
    	fileWriter.close();
    }
    
    /**
     * Adds all custom tools to the data model.
     * 
     * @param dataModel
     */
    private void addTools(Map<String, Object> dataModel)
    {
    	if(toolFactoryList != null)
    	{
    		for(ToolFactory<?> toolFactory : toolFactoryList)
    		{
    			FreemarkerTool freemarkerTool = toolFactory.getInstance(null);
    			
    			if(freemarkerTool != null)
    			{
    				dataModel.put(freemarkerTool.getName(), freemarkerTool);
    			}
    		}
    	}
    }
}
