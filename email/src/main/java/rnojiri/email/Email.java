package rnojiri.email;

import java.io.File;
import java.util.Map;
import java.util.Set;

/**
 * The e-mail POJO.
 * 
 * @author rnojiri
 */
public class Email
{
	private String from;
	
	private Set<String> to;
	
	private Set<String> cc;
	
	private Set<String> bcc;
	
	private String subject;
	
	private Map<String, Object> variableMap;
	
	private String template;
	
	private ContentType contentType;
	
	private File attachment;
	
	/**
	 * Allowed content types.
	 * 
	 * @author rnojiri
	 */
	public enum ContentType
	{
		TEXT_PLAIN("text/plain; charset=utf-8"),
		
		TEXT_HTML("text/html; charset=utf-8"),
		
		APPLICATION_OCTET_STREAM("application/octet-stream");
		
		private final String value;
		
		private ContentType(final String value)
		{
			this.value = value;
		}
		
		public String getValue()
		{
			return value;
		}
	}
	
	public Email()
	{
		;
	}

	/**
	 * @return the from
	 */
	public String getFrom()
	{
		return from;
	}

	/**
	 * @param from the from to set
	 */
	public void setFrom(String from)
	{
		this.from = from;
	}

	/**
	 * @return the to
	 */
	public Set<String> getTo()
	{
		return to;
	}

	/**
	 * @param to the to to set
	 */
	public void setTo(Set<String> to)
	{
		this.to = to;
	}

	/**
	 * @return the cc
	 */
	public Set<String> getCc()
	{
		return cc;
	}

	/**
	 * @param cc the cc to set
	 */
	public void setCc(Set<String> cc)
	{
		this.cc = cc;
	}

	/**
	 * @return the bcc
	 */
	public Set<String> getBcc()
	{
		return bcc;
	}

	/**
	 * @param bcc the bcc to set
	 */
	public void setBcc(Set<String> bcc)
	{
		this.bcc = bcc;
	}

	/**
	 * @return the subject
	 */
	public String getSubject()
	{
		return subject;
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject)
	{
		this.subject = subject;
	}

	/**
	 * @return the variablesMap
	 */
	public Map<String, Object> getVariableMap()
	{
		return variableMap;
	}

	/**
	 * @param variableMap the variableMap to set
	 */
	public void setVariableMap(Map<String, Object> variableMap)
	{
		this.variableMap = variableMap;
	}

	/**
	 * @return the template
	 */
	public String getTemplate()
	{
		return template;
	}

	/**
	 * @param template the template to set
	 */
	public void setTemplate(String template)
	{
		this.template = template;
	}

	/**
	 * @return the contentType
	 */
	public ContentType getContentType()
	{
		return contentType;
	}

	/**
	 * @param contentType the contentType to set
	 */
	public void setContentType(ContentType contentType)
	{
		this.contentType = contentType;
	}
	
	@Override
	public String toString()
	{
		return new StringBuilder("{ \"email\" : { \"")
									.append("\"from\" : \"").append(from).append("\", ")
									.append("\"to\" : \"").append((to != null ? to.toString() : "[]")).append("\", ")
									.append("\"cc\" : \"").append((cc != null ? cc.toString() : "[]")).append("\", ")
									.append("\"bcc\" : \"").append((bcc != null ? cc.toString() : "[]")).append("\", ")
									.append("\"subject\" : \"").append(subject).append("\", ")
									.append("\"template\" : \"").append(template).append("\"")
						 .append("} }").toString();
	}

	/**
	 * @return the attachment
	 */
	public File getAttachment()
	{
		return attachment;
	}

	/**
	 * @param attachment the attachment to set
	 */
	public void setAttachment(File attachment)
	{
		this.attachment = attachment;
	}
}
