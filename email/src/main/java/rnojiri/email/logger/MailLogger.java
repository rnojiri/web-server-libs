package rnojiri.email.logger;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;

import rnojiri.email.Email;
import rnojiri.email.MailSender;
import rnojiri.lib.support.Constants;

/**
 * A mail logger sent to the tech-team members.
 * 
 * @author rnojiri
 */
public class MailLogger
{
	private static final int MAX_STACKTRACE_ELEMENTS = 20;
	
	private static final String ARROW_SEPARATOR = " -> ";
	
	private static final String MESSAGE = "message";

	private final MailSender mailSender;
	
	private final Email baseEmail;
	
	private enum LogCriticity
	{
		INFO,
		WARN,
		ERROR,
		DEBUG
	}

	/**
	 * @param mailSender
	 * @param baseEmail
	 */
	public MailLogger(MailSender mailSender, Email baseEmail)
	{
		super();
		
		this.mailSender = mailSender;
		this.baseEmail = baseEmail;
	}
	
	/**
	 * Logs a warn message.
	 * 
	 * @param message
	 * @param e
	 * @param logger
	 */
	public void warn(String message, Throwable e, Logger logger)
	{
		logger.warn(message, e);
		
		sendEmail(LogCriticity.WARN, message, e);
	}
	
	/**
	 * Logs a error message.
	 * 
	 * @param message
	 * @param e
	 * @param logger
	 */
	public void error(String message, Throwable e, Logger logger)
	{
		logger.error(message, e);
		
		sendEmail(LogCriticity.ERROR, message, e);
	}
	
	/**
	 * Logs an info message.
	 * 
	 * @param message
	 * @param e
	 * @param logger
	 */
	public void info(String message, Throwable e, Logger logger)
	{
		logger.info(message, e);
		
		sendEmail(LogCriticity.INFO, message, e);
	}
	/**
	 * Logs info message for mailChannel
	 * @param json
	 * @param logger
	 * @param b
	 */

	/**
	 * Logs a debug message.
	 * 
	 * @param message
	 * @param e
	 * @param logger
	 */
	public void debug(String message, Throwable e, Logger logger)
	{
		logger.debug(message, e);
		
		sendEmail(LogCriticity.DEBUG, message, e);
	}
	
	/**
	 * Logs a warn message.
	 * 
	 * @param message
	 * @param logger
	 */
	public void warn(String message, Logger logger)
	{
		logger.warn(message);
		
		sendEmail(LogCriticity.WARN, message, null);
	}
	
	/**
	 * Logs a error message.
	 * 
	 * @param message
	 * @param logger
	 */
	public void error(String message, Logger logger)
	{
		logger.error(message);
		
		sendEmail(LogCriticity.ERROR, message, null);
	}
	
	/**
	 * Logs a info message.
	 * 
	 * @param message
	 * @param logger
	 */
	public void info(String message, Logger logger)
	{
		logger.info(message);
		
		sendEmail(LogCriticity.INFO, message, null);
	}
	
	/**
	 * Logs a debug message.
	 * 
	 * @param message
	 * @param e
	 * @param logger
	 */
	public void debug(String message, Logger logger)
	{
		logger.debug(message);
		
		sendEmail(LogCriticity.DEBUG, message, null);
	}

	/**
	 * Sends the message to e-mail.
	 * 
	 * @param logCriticity
	 * @param message
	 * @param e
	 */
	private void sendEmail(LogCriticity logCriticity, String message, Throwable e)
	{
		try
		{
			Email copy = new Email();
			
			BeanUtils.copyProperties(baseEmail, copy);
			
			Map<String, Object> variableMap = new HashMap<String, Object>();
			
			variableMap.put(MESSAGE, buildLogMessage(logCriticity, message, e));
			
			copy.setVariableMap(variableMap);
			
			mailSender.sendMail(copy, false);
		}
		catch(Exception internalException)
		{
			throw new RuntimeException(internalException);
		}
	}
	
	/**
	 * Builds the log message.
	 * 
	 * @param logCriticity
	 * @param message
	 * @param e
	 * @return String
	 */
	private String buildLogMessage(LogCriticity logCriticity, String message, Throwable e)
	{
		StringBuilder builder = new StringBuilder(Constants.BUFFER_SIZE_1024);
		
		builder.append('[').append(logCriticity.name()).append(']').append('\n');
		
		if(StringUtils.isNotBlank(message))
		{
			builder.append(message).append('\n');
		}
		
		if(e != null)
		{
			builder.append(ExceptionUtils.getStackTrace(e));
		}
		else
		{
			StackTraceElement stackTraceElements[] = Thread.currentThread().getStackTrace();
			
			for(int i=3; i<MAX_STACKTRACE_ELEMENTS && i<stackTraceElements.length; i++)
			{
				if(StringUtils.isNotBlank(stackTraceElements[i].getFileName()))
				{
					builder.append('[').append(stackTraceElements[i].getFileName()).append(':')
						   .append(stackTraceElements[i].getLineNumber()).append(']').append(' ')
						   .append(stackTraceElements[i].getClassName()).append(ARROW_SEPARATOR)
						   .append(stackTraceElements[i].getMethodName()).append('\n');
				}
			}
		}
		
		return builder.toString();
	}

}
