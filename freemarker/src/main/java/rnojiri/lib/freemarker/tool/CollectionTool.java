package rnojiri.lib.freemarker.tool;

import java.util.Collection;
import java.util.Map;

/**
 * A tool for handle collections. 
 * 
 * @author rnojiri
 */
public class CollectionTool implements FreemarkerTool
{
	private static final String NAME = "collectionTool"; 
	
	public CollectionTool()
	{
		;
	}
	
	/**
	 * Checks for a key inside the specified map.
	 * 
	 * @param map
	 * @param key
	 * @return boolean
	 */
	public boolean hasKey(Map<?, ?> map, Object key)
	{
		if(map != null)
		{
			return map.containsKey(key);
		}
		
		return false;
	}
	
	/**
	 * Checks for an object inside the specified collection.
	 * 
	 * @param set
	 * @param obj
	 * @return boolean
	 */
	public boolean hasObject(Collection<?> collection, Object obj)
	{
		if(collection != null)
		{
			return collection.contains(obj);
		}
		
		return false;
	}
	
	/**
	 * Returns the size from a map.
	 * 
	 * @param map
	 * @return int
	 */
	public int getMapSize(Map<?, ?> map)
	{
		if(map != null)
		{
			return map.size();
		}
		
		return 0;
	}
	
	/**
	 * Returns the size from a collection.
	 * 
	 * @param collection
	 * @return int
	 */
	public int getCollectionSize(Collection<?> collection)
	{
		if(collection != null)
		{
			return collection.size();
		}
		
		return 0;
	}
	
	@Override
	public String getName()
	{
		return NAME;
	}
}
