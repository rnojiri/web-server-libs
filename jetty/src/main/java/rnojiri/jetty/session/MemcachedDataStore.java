package rnojiri.jetty.session;

import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.jetty.server.session.AbstractSessionDataStore;
import org.eclipse.jetty.server.session.SessionData;
import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Logger;

import rnojiri.lib.cache.CacheInstance;

public class MemcachedDataStore extends AbstractSessionDataStore implements CustomSessionDataStore
{

	private static final Logger LOGGER = Log.getLogger(MemcachedDataStore.class);

	private final int ttl;

	private CacheInstance cacheInstance;

	/**
	 * 
	 * @param cacheInstance
	 * @param params: TTL do dado
	 */
	public MemcachedDataStore(String stringTtl)
	{
		this.ttl = Integer.parseInt(stringTtl);
	}

	@Override
	public boolean isPassivating()
	{
		return false;
	}

	@Override
	public boolean exists(String id) throws Exception
	{
		return cacheInstance.get(id) != null;
	}

	@Override
	public boolean delete(String id) throws Exception {
		return cacheInstance.remove(id);
	}

	@Override
	public void doStore(String id, SessionData data, long lastSaveTime) throws Exception {
		cacheInstance.set(id, ttl, data);
	}

	@Override
	public SessionData doLoad(String id) throws Exception {
		return cacheInstance.get(id);
	}

	@Override
	public Set<String> doGetExpired(Set<String> candidates) {
		return candidates.stream().filter(candidate -> {
			boolean isOnCache = false;
			try
			{
				isOnCache = cacheInstance.get(candidate) != null;
			} catch (Exception e)
			{
				LOGGER.info("Exception occurred while getting {0}", candidate);
			}
			return isOnCache;
		}).collect(Collectors.toSet());
	}

	@Override
	public void setCacheInstance(CacheInstance cacheInstance)
	{
		this.cacheInstance = cacheInstance;
	}

}
